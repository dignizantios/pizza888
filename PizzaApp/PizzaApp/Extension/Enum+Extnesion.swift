//
//  Enum+Extnesion.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation

enum selectNavigation : Int {
    case sidemenu = 1
    case back = 2
    case sideMenuWithCart = 3
    case backWithCart = 4
    case withoutBack = 5
    case clearAll = 6
}

enum navigationType
{
    case black
    case white
    case clear
}

enum selectedPickupTime : Int
{
    case asap = 1
    case future = 2
}

enum selectOrderType : Int{
    case delivery = 1
    case pickup = 2
}

enum selectPolicyType : Int
{
    case TermsCondition = 1
    case PrivacyPolicy = 2
    case DeliveryPolicy = 3
    case RefundPolicy = 4
    case AboutUs = 5
}

enum selectPolicyRedirectVC
{
    case register
    case helpSetting
}

enum selectProfile
{
    case register
    case editProfile
}

enum selectPizzaType {
    case suace
    case subType
    case type
}

enum selectCategoryItem {
    case regular
//    case pasta
    case deal
    case half
    case drink
}

enum selectPaymentType:String {
    case creditCard = "1"
    case paypal = "2"
    case cash = "3"
}

enum selectMyOrderType:String {
    case future = "2"
    case past = "1"
}

enum dateFormatter:String {
    case dateFormatter1 = "yyyy-MM-dd HH:mm:ss"
    case dateFormatter2 = "dd-MM-yyyy HH:mm"
}

enum selectPaymentOrderType{
    case paypal
    case order
}

enum subType:String {
    case base = "Base"
    case size = "Size"
    case sauce = "Sauce"
}

