//
//  UIViewController + Extension.swift
//  PizzaApp
//
//  Created by DK on 09/10/17.
//  Copyright © 2017 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import DropDown
import AVFoundation
import UserNotifications
import NVActivityIndicatorView
import RealmSwift

extension UIViewController:NVActivityIndicatorViewable
{
    /*var GlobalCartCount = 0 {
        
        didSet {
            if let viewWithTag = self.navigationController?.view.viewWithTag(100) as? ViewNav
            {
                if(GetCartData().count > 0)
                {
                    viewWithTag.lblCartCount.text = GlobalCartCount == GetCartItemsCount()
                }
                
            }
            
        }
        
    }*/

    //MARK: - StartAnimating
    
    func showLoader()
    {
        self.view.endEditing(true)
        startAnimating(Loadersize, message:strLoader , type: NVActivityIndicatorType.ballSpinFadeLoader)
    }
    
    func stopLoader()
    {
        self.stopAnimating()
    }

    //MARK: - Methods
    func hideKeyboardDismiss()
    {
        self.view.endEditing(true)
    }
    
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Done button on keyboard
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.appThemeRedColor
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    //MARK: - Navigation bar setup
    
    func setUpNavigationBarWithForgotPassword(strTitle : String)
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.isTranslucent = true
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_green"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.appThemeGreenColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeGreenColor
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    } 
    
    
    func setUpNavigationBarWithBack()
    {

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.isTranslucent = true
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_green"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.appThemeGreenColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
    }
    
    func setUpNavigationBarWithTitleAndBack(strTitle : String) 
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true

        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_red_arrow"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.appThemeRedColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeRedColor
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)

        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithTitleAndBackAndReport(strTitle : String)
    {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeGreenColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        UINavigationBar.appearance().setBackgroundImage(navImage, for: .default)
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let rightButton = UIBarButtonItem(image: UIImage.init(named: "ic_report_spam_header"), style: .plain, target: self, action: #selector(btnSpamAction))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithTitle(strTitle : String)
    {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeGreenColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        UINavigationBar.appearance().setBackgroundImage(navImage, for: .default)
    
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithTitleAndWithOutBack(strTitle : String)
    {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        self.navigationItem.hidesBackButton = true
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeGreenColor
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    @objc func backButtonAction(_ sender : UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnSpamAction(_ sender : UIBarButtonItem)
    {
       // let obj = objUserHomeStoryboard.instantiateViewController(withIdentifier: "ReportSpamCoachVC") as! ReportSpamCoachVC
       // self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle : String,type:selectNavigation,barType:navigationType)
    {
        let bounds = self.navigationController!.navigationBar.bounds
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + navHeight)
        
       // self.navigationController?.isNavigationBarHidden = false  
        
        if let vwnav = Bundle.main.loadNibNamed("ViewNav", owner: nil, options: nil)?[0] as? ViewNav

        {
            var hightOfView:CGFloat = 0.0
            
            if UIScreen.main.bounds.height >= 812
            {
                hightOfView = 20
            }            
            
            vwnav.frame = CGRect(x: 0, y: 0, width: self.navigationController?.navigationBar.frame.width ?? 320, height: vwnav.frame.height+hightOfView)
            
            if barType == .white {
                vwnav.imgHeader.image = UIImage(named: "bh_header_shadow")
            } else if barType == .clear{
                vwnav.imgHeader.image = UIImage(named: "ic_header")
                vwnav.backgroundColor = .clear
                self.navigationController?.navigationBar.tintColor = .clear
            }
            vwnav.lblTitle.text = strTitle
            vwnav.btnRightOutlet.isHidden = true
            vwnav.lblCartCount.isHidden = true
            vwnav.btnLeftOutlet.tag = type.rawValue
            
            let checkedCartCount = GetCartItemsCount()
            
            if type == .sideMenuWithCart
            {
                if checkedCartCount == 0
                {
                    vwnav.lblCartCount.isHidden = true
                }
                else
                {
                    vwnav.lblCartCount.isHidden = false
                    vwnav.lblCartCount.text = String(checkedCartCount)
                }
                vwnav.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
               
                vwnav.btnRightOutlet.isHidden = false
                vwnav.btnRightOutlet.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
            }
            else if type == .back
            {
                vwnav.btnRightOutlet.isHidden = true
                vwnav.btnLeftOutlet.setImage(UIImage(named: "ic_back_arrow_header"), for: .normal)
                vwnav.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
            else if type == .sidemenu
            {
                vwnav.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
            else if type == .backWithCart
            {                
                if checkedCartCount == 0
                {
                    vwnav.lblCartCount.isHidden = true
                }
                else
                {
                    vwnav.lblCartCount.isHidden = false
                    vwnav.lblCartCount.text = String(checkedCartCount)
                }
                
                vwnav.btnRightOutlet.isHidden = false
                vwnav.btnRightOutlet.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
                vwnav.btnLeftOutlet.setImage(UIImage(named: "ic_back_arrow_header"), for: .normal)
                vwnav.btnLeftOutlet.isHidden = false
                vwnav.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
            else if type == .withoutBack {
                vwnav.btnRightOutlet.isHidden = true
                vwnav.btnLeftOutlet.isHidden = true
            } else if type == .clearAll {
                
                vwnav.btnLeftOutlet.setImage(UIImage(named: "ic_back_arrow_header"), for: .normal)
                vwnav.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
                
                vwnav.btnRightOutlet.isHidden = false
                vwnav.btnRightOutlet.setImage(nil, for: .normal)
                vwnav.btnRightOutlet.setTitle(getCommonString(key: "Clear_all_key").uppercased(), for: .normal)
                vwnav.btnRightOutlet.setTitleColor(.white, for: .normal)
                vwnav.btnRightOutlet.titleLabel?.font = themeFont(size: 16, fontname: .semibold)
                vwnav.btnRightOutlet.addTarget(self, action: #selector(btnDeleteAllCartAction(_:)), for: .touchUpInside)
            }
            self.navigationController?.view.addSubview(vwnav)
        }
        
    }
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
//        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width + 50
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    func getUserDetail(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "userDetail") as? Data else { return "" }
        let data = JSON(userDetail)
     //   print("data \(data)")
        return data[forKey].stringValue
    }
    
    func logoutAPICalling()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
    }
    
   /* func getOnlyPinUserDetail(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "customDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        //   print("data \(data)")
        return data[forKey].stringValue
    }*/
    
    @objc func openLeftViewController() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSidemenu"), object: nil)
        //sideMenuController?.leftViewController?.showLeftViewAnimated(true)
    }
    
    
    
    
    //MARK: - Button Action
    @IBAction func btnBackSideAction(_ sender : UIButton)
    {
        if sender.tag == 2 || sender.tag == 4 || sender.tag == 6
        {
             self.navigationController?.popViewController(animated: true)
        }
        else{
            sideMenuController?.leftViewController?.showLeftViewAnimated(true)
        }
       
    }
   
    @IBAction func btnCartAction(_ sender : UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnDeleteAllCartAction(_ sender : UIButton)
    {
        
    }
    
    //MARK:- Email Adderess validation
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9]+\\.[A-Za-z]{2,64}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func setAttributedText(text:String,font:UIFont,color:UIColor) -> NSMutableAttributedString {
        
        let attrs = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : color] as [NSAttributedString.Key : Any]
        
        let strAttributed = NSMutableAttributedString()
        let buttonTitleStr =  NSMutableAttributedString(string:text, attributes:attrs)
        strAttributed.append(buttonTitleStr)
        return strAttributed
    }
    //MARK:- Cart Add Values
    func AddToCart(dict : JSON)
    {
        dictGlobalCartData.append(dict)
        print("Global Cart Dict - ",dictGlobalCartData)
        
        let PKId = cartIncrementID()
        
        if(dict["type"].stringValue == "2" || dict["type"].stringValue == "1")
        {
            let objCart = Cart().storeData(dict: dict,PKId : PKId)
            try! globalRealm.write
            {
                globalRealm.add(objCart)
            }
        }else if(dict["type"].stringValue == "3")
        {
            print("dict - ",dict)

            let objCart = Cart().storeData(dict: dict,PKId : PKId)
            try! globalRealm.write
            {
                globalRealm.add(objCart)
                
                let arrayDealItems = dict["dealData"].arrayValue
                arrayDealItems.forEach { (json) in
                    
                    print("Deal item json - ",json)
                    
                    let PKItemId = cartItemsIncrementID()
                    let objCartItems = CartDealItems().storeData(dict: json,PKId : PKItemId,cartId:PKId)
                    globalRealm.add(objCartItems)
                }
            }            
        }
        else if(dict["type"].stringValue == "4")
        {
            print("Half half dict - ",dict)
            
            let objCart = Cart().storeHalfHalfData(dict: dict, PKId: PKId)
            try! globalRealm.write
            {
                globalRealm.add(objCart)
                
                let dict1 = dict["Half1"]
                
                let PKItem1Id = cartItemsIncrementID()
                let objCartItems1 = CartDealItems().storeData(dict: dict1,PKId : PKItem1Id,cartId:PKId)
                globalRealm.add(objCartItems1)
                
                let dict2 = dict["Half2"]
                
                let PKItem2Id = cartItemsIncrementID()
                let objCartItems2 = CartDealItems().storeData(dict: dict2,PKId : PKItem2Id,cartId:PKId)
                globalRealm.add(objCartItems2)
                
            }
        }
        
//        GlobalCartCount = GetCartItemsCount()
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Select_items_key"), type: .backWithCart, barType: .white)
        vwnav?.lblCartCount.text = "\(GetCartData().count)"
        
        
       /* let vwnav = Bundle.main.loadNibNamed("ViewNav", owner: nil, options: nil)?[0] as? ViewNav
        GlobalCartPrice = CartTotalPrice()
        vwNavigationBar?.lblCartCount.text = "\(GetCartData().count)"*/
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetTotalPriceValue"), object: nil)
        
        makeToast(message: getValidationString(key: "Add_to_cart_successfull_key"))
        
    }
    
    func cartIncrementID() -> Int
    {
        print("cart id - ",Cart.primaryKey())
        return (globalRealm.objects(Cart.self).max(ofProperty: Cart.primaryKey()) as Int? ?? 0) + 1
    }
    func cartItemsIncrementID() -> Int
    {
        print("cart item id - ",CartDealItems.primaryKey())
        return (globalRealm.objects(CartDealItems.self).max(ofProperty: CartDealItems.primaryKey()) as Int? ?? 0) + 1
    }
    func checkoutUserIncrementID() -> Int
    {
        return (globalRealm.objects(CheckoutUser.self).max(ofProperty: CheckoutUser.primaryKey()) as Int? ?? 0) + 1
    }
    func checkoutOrderIncrementID() -> Int
    {
        return (globalRealm.objects(CheckoutOrder.self).max(ofProperty: CheckoutOrder.primaryKey()) as Int? ?? 0) + 1
    }
    func GetCartItemsCount() -> Int
    {
        let checkedCartItems = Array(globalRealm.objects(Cart.self))
        return checkedCartItems.count
    }
    func GetCartData() -> [JSON]
    {
        
        /*
         "baseType" : "Thin",
         "half_price" : "14.50",
         "small_price" : "11.50",
         "remove_ingr_name" : "Chicken,Spanish Onions",
         "specification" : "Chicken, spinach, tomatoes, Spanish Onions, kalamata olives and feta cheese",
         "price" : "18.00",
         "defaultTypeId" : "2",
         "is_customized" : "0",
         "product_name" : "Greek",
         "ingr_add" : "2,3",
         "extra_description" : "",
         "remove_ingr_id" : "8,32",
         "pizzaSize" : "Small",
         "addedTopingsPrice" : "4.0",
         "type" : "2",
         "sauce_type" : "BBQ Sauce",
         "ingr_name" : "Rasher Bacon,Virginia Ham",
         "description" : "Less spicy pizza"
 
        */
        
        
        var arrayCartItems : [JSON] = []
        let checkedCartItems = Array(globalRealm.objects(Cart.self))

        if(checkedCartItems.count > 0)
        {
            
            checkedCartItems.forEach { (objCart) in
                
                var json = JSON()
                
                json["cart_id"] = JSON(objCart.id)
                json["product_id"] = JSON(objCart.product_id)
                json["baseType"] = JSON(objCart.product_baseType)
                json["half_price"] = JSON(objCart.product_half_price)
                json["small_price"] = JSON(objCart.product_small_price)
                json["remove_ingr_name"] = JSON(objCart.remove_ingr_name)
                json["specification"] = JSON(objCart.specification)
                json["price"] = JSON(objCart.product_price)
                json["defaultTypeId"] = JSON(objCart.defaultTypeId)
                json["product_name"] = JSON(objCart.product_name)
                json["ingr_add"] = JSON(objCart.ingr_add)
                json["extra_description"] = JSON(objCart.product_extra_description)
                json["remove_ingr_id"] = JSON(objCart.remove_ingr_id)
                json["pizzaSize"] = JSON(objCart.pizzaSize)
                json["addedTopingsPrice"] = JSON(objCart.addedTopingsPrice)
                json["type"] = JSON(objCart.product_type)
                json["sauce_type"] = JSON(objCart.product_sauce_type)
                json["ingr_name"] = JSON(objCart.ingr_name)
                json["description"] = JSON(objCart.product_description)
                json["customized"] = JSON(objCart.isCustomized)
                json["category_name"] = JSON(objCart.product_category)
                json["isTypePasta"] = JSON(objCart.isTypePasta)
                json["finalPrice"] = JSON(objCart.finalPrice)
                json["addedVariant"] = JSON(objCart.addedVariant)
                json["quantity"] = JSON(objCart.quantity)
                json["category_name"] = JSON(objCart.category_name)
//                json["category_id"] = JSON(objCart.pr)

                json["dealid"] = JSON(objCart.deal_id)
                json["dealName"] = JSON(objCart.deal_name)
                json["dealdiscription"] = JSON(objCart.deal_description)
                json["dealPrice"] = JSON(objCart.deal_price)
                
                
                json["half_id"] = JSON(objCart.half_id)
                json["halfName"] = JSON(objCart.half_name)
                json["halfdiscription"] = JSON(objCart.half_description)
                json["half_price"] = JSON(objCart.half_price)
                
                if(objCart.product_type == "3" || objCart.product_type == "4")
                {
                    
                    let checkedDealCartItems = Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == objCart.id}
                    if(checkedDealCartItems.count > 0)
                    {
                        var arraySubItems:[JSON] = []
                        checkedDealCartItems.forEach({ (objCartSubItems) in
                            
                        
                            var dictJson = JSON()
                            dictJson["id"] = JSON(objCartSubItems.id)

                            dictJson["cart_id"] = JSON(objCartSubItems.cart_id)
                            dictJson["product_id"] = JSON(objCartSubItems.product_id)
                            dictJson["baseType"] = JSON(objCartSubItems.product_baseType)
                            dictJson["half_price"] = JSON(objCartSubItems.product_half_price)
                            dictJson["small_price"] = JSON(objCartSubItems.product_small_price)
                            dictJson["remove_ingr_name"] = JSON(objCartSubItems.remove_ingr_name)
                            dictJson["specification"] = JSON(objCartSubItems.specification)
                            dictJson["price"] = JSON(objCartSubItems.product_price)
                            dictJson["defaultTypeId"] = JSON(objCartSubItems.defaultTypeId)
                            dictJson["product_name"] = JSON(objCartSubItems.product_name)
                            dictJson["ingr_add"] = JSON(objCartSubItems.ingr_add)
                            dictJson["extra_description"] = JSON(objCartSubItems.product_extra_description)
                            dictJson["remove_ingr_id"] = JSON(objCartSubItems.remove_ingr_id)
                            dictJson["pizzaSize"] = JSON(objCartSubItems.pizzaSize)
                            dictJson["addedTopingsPrice"] = JSON(objCartSubItems.addedTopingsPrice)
                            dictJson["type"] = JSON(objCartSubItems.product_type)
                            dictJson["sauce_type"] = JSON(objCartSubItems.product_sauce_type)
                            dictJson["ingr_name"] = JSON(objCartSubItems.ingr_name)
                            dictJson["description"] = JSON(objCartSubItems.product_description)
                            dictJson["customized"] = JSON(objCartSubItems.isCustomized)
                            dictJson["addedVariant"] = JSON(objCartSubItems.addedVariant)
                            
                            arraySubItems.append(dictJson)
                        })
                        
                        json["sub_data"] = JSON(arraySubItems)
                    }

                }
                
                arrayCartItems.append(json)
            }
            
        }
        print("arrayCartItems - ",arrayCartItems)
        
        return arrayCartItems
    }
    func CartTotalPrice() -> Float
    {
        let arrayCart = GetCartData()
        
        var totalPrice : Float = 0.0
        arrayCart.forEach { (json) in
            
            if(json["type"].stringValue == "2")
            {
//                totalPrice = totalPrice + getPrice(dict:json)
                totalPrice += getFinalPrice(dict: json)
                print("totalPrice type2 - ",totalPrice)
            }
            else if(json["type"].stringValue == "1")
            {
//                totalPrice = totalPrice + json["price"].floatValue
                totalPrice += getFinalPrice(dict: json)
                print("totalPrice type1 - ",totalPrice)
                
            }
            else if(json["type"].stringValue == "3")
            {
                totalPrice = totalPrice + json["dealPrice"].floatValue
                print("totalPrice type3 - ",totalPrice)
                
            }
            else if(json["type"].stringValue == "4")
            {
                totalPrice = totalPrice + json["half_price"].floatValue
                print("totalPrice type4 - ",totalPrice)
                
            }
            
        }
        
        return totalPrice
        
    }
    func  getPrice(dict:JSON) -> Float
    {
        let id = dict["defaultTypeId"].stringValue
        var addedPrice : Float = 0.0
        
        
        if(dict["isTypePasta"].stringValue == "1")
        {
            addedPrice = dict["product_price"].floatValue
            let toppingPrice = dict["addedTopingsPrice"].floatValue
            addedPrice = addedPrice + toppingPrice
            return addedPrice
        }        
        
        /*switch id
        {
        case "0":
            addedPrice = dict["half_price"].floatValue
        case "1":
            addedPrice = dict["price"].floatValue
        case "2":
            addedPrice = dict["small_price"].floatValue
        default:
            addedPrice = dict["price"].floatValue
        } */
        addedPrice = dict["product_price"].floatValue
        let toppingPrice = dict["addedTopingsPrice"].floatValue
        addedPrice = addedPrice + toppingPrice
        
        print("addedPrice - ",addedPrice)
        return addedPrice
        
    }
    
    func getFinalPrice(dict:JSON) -> Float{
        let id = dict["defaultTypeId"].stringValue
        var addedPrice : Float = 0.0
        
        
        if(dict["isTypePasta"].stringValue == "1")
        {
            addedPrice = dict["finalPrice"].floatValue * dict["quantity"].floatValue
//            let toppingPrice = dict["addedTopingsPrice"].floatValue
//            addedPrice = addedPrice + toppingPrice
            return addedPrice
        }
        addedPrice = dict["finalPrice"].floatValue * dict["quantity"].floatValue
        
        return addedPrice
    }
    
    func DeleteCart()
    {
        
        //
        let cartObjects =  Array(globalRealm.objects(Cart.self))
        try! globalRealm.write
        {
            if cartObjects.count > 0
            {
                
                cartObjects.forEach({ (objCart) in
                    globalRealm.delete(objCart)
                })
                
            }
            
            let cartSubItemsObjects =  Array(globalRealm.objects(CartDealItems.self))
            if cartSubItemsObjects.count > 0
            {
                cartSubItemsObjects.forEach({ (objCartDealItems) in
                    globalRealm.delete(objCartDealItems)
                })
            }
        }
    }
    func DeleteCartItems(cartId : Int,type:Int)
    {
        
        //
        let cartObjects =  Array(globalRealm.objects(Cart.self)).filter { $0.id == cartId}
        try! globalRealm.write
        {
            if cartObjects.count > 0
            {
                
                cartObjects.forEach({ (objCart) in
                    globalRealm.delete(objCart)
                })
                
            }
//            if(type != 3)
//            {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetTotalPriceValue"), object: nil)
//                return
//            }
            
            let cartSubItemsObjects =  Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == cartId}
            if cartSubItemsObjects.count > 0
            {
                cartSubItemsObjects.forEach({ (objCartDealItems) in
                    globalRealm.delete(objCartDealItems)
                })
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetTotalPriceValue"), object: nil)
        }
    }
    func GetUserData() -> User
    {
        let objUserLocal = User()
        let checkedUser = Array(globalRealm.objects(CheckoutUser.self))
        if checkedUser.count != 0
        {
            let objFirst  = checkedUser.first!
            try? globalRealm.write
            {
                objUserLocal.strName = objFirst.strName
                objUserLocal.strPhoneNumber = objFirst.strPhoneNumber
                objUserLocal.strEmailAddress = objFirst.strEmailAddress
                objUserLocal.strUnitNumber = objFirst.strUnitNumber
                objUserLocal.strStreetNumber = objFirst.strStreetNumber
                objUserLocal.strStreetName = objFirst.strStreetName
                objUserLocal.strSuburbName = objFirst.strSuburbName
                objUserLocal.strDeliveryInsturction = objFirst.strDeliveryInsturction
                objUserLocal.strRememberPickupDetil = objFirst.strRememberPickupDetil               
                
            }
        }
        
        return objUserLocal
        
    }
    func GetOrderData() -> Order
    {
        let objOrderLocal = Order()
        let checkedOrder = Array(globalRealm.objects(CheckoutOrder.self))
        if checkedOrder.count != 0
        {
            let objFirst  = checkedOrder.first!
            try? globalRealm.write
            {
                objOrderLocal.strFutureOrderDate = objFirst.strFutureOrderDate
                objOrderLocal.strFutureOrderTime = objFirst.strFutureOrderTime
                objOrderLocal.strCouponCode = objFirst.strCouponCode
                objOrderLocal.isSelectPaymentOption = objFirst.isSelectPaymentOption
                objOrderLocal.strSelectPaymentOption = objFirst.strSelectPaymentOption
                objOrderLocal.strSelectStore = objFirst.strSelectStore
                objOrderLocal.strOrderNumber = objFirst.strOrderNumber
                objOrderLocal.strComment = objFirst.strComment
                objOrderLocal.strSelectedOrderType = objFirst.strSelectedOrderType == selectOrderType.pickup.rawValue ? selectOrderType.pickup : selectOrderType.delivery
                objOrderLocal.strSelectedPickUpTime = objFirst.strSelectedPickUpTime == selectedPickupTime.asap.rawValue ? selectedPickupTime.asap : selectedPickupTime.future
                objOrderLocal.strSelectedStoreId = objFirst.strSelectedStoreId
                objOrderLocal.strStorePhoneNumber = objFirst.strStorePhoneNumber
                objOrderLocal.strStoreAddress = objFirst.strStoreAddress
            }
        }
        
        return objOrderLocal
    }
    
    //MARK:- Get Data
    
    func GetCheckOutCartItem() -> JSON
    {
        var dictCheckoutJson = JSON()
        
        var arrayDrinkItems : [JSON] = []
        var arrayFullPizzaItems : [JSON] = []
        var arrayDealItems : [JSON] = []
        var arrayHalfHalfItems : [JSON] = []
        
        let arrayDirectAddedCartItems = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "1"}
        print("")
        if(arrayDirectAddedCartItems.count > 0)
        {
            arrayDirectAddedCartItems.forEach { (objCart) in
                //                arrayFullPizzaItems.append(objCart.GetData())
                
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.product_name)
                dictPizza["prdid"] = JSON(objCart.product_id)
                dictPizza["qty"] = JSON(objCart.quantity)
                dictPizza["prdingrd"] = JSON(objCart.ingr_name)
                dictPizza["removetop"] = JSON(objCart.remove_ingr_name)
                dictPizza["comment"] = JSON(objCart.product_description)
                dictPizza["variant"] = ""
                if objCart.isCustomized == "1" {
                    dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPrice(objCart: objCart)))
                } else {
                    dictPizza["prdvalue"] = JSON(objCart.product_price)
                }
                arrayFullPizzaItems.append(dictPizza)
            }
            
        }
        
        let arrayPizzaItems = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "2"}
        if(arrayPizzaItems.count > 0)
        {
            arrayPizzaItems.forEach { (objCart) in
                
//                let strVarient = "\(objCart.pizzaSize),\(objCart.product_sauce_type),\(objCart.product_baseType)"
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.product_name)
                dictPizza["prdid"] = JSON(objCart.product_id)
                dictPizza["qty"] = JSON(objCart.quantity)
                dictPizza["prdingrd"] = JSON(objCart.ingr_name)
                dictPizza["removetop"] = JSON(objCart.remove_ingr_name)
                //                dictPizza["pbase"] = JSON(objCart.product_baseType)
                //                dictPizza["psaurce"] = JSON(objCart.product_sauce_type)
                //                dictPizza["psize"] = JSON(objCart.pizzaSize)
                dictPizza["comment"] = JSON(objCart.product_description)
                dictPizza["variant"] = JSON(objCart.addedVariant)
                
                if objCart.isCustomized == "1" {
                    dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPrice(objCart: objCart)))
                } else {
                    dictPizza["prdvalue"] = JSON(objCart.product_price)
                }
                arrayFullPizzaItems.append(dictPizza)
            }
            
        }
        
        let arrayDeals = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "3"}
        if(arrayDeals.count > 0)
        {
            arrayDeals.forEach { (objCart) in
                
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.deal_name)
                dictPizza["prdid"] = JSON(objCart.deal_id)
                dictPizza["prdvalue"] = JSON(objCart.deal_price)
                dictPizza["qty"] = "1"
                dictPizza["rmingr"] = ""
                dictPizza["pcomnt"] = JSON(objCart.deal_extra_instruction)
                var arraySubData : [JSON] = []
                
                let arrayDealsSubItems = Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == objCart.id}
                if(arrayDealsSubItems.count > 0)
                {
                    
                    arrayDealsSubItems.forEach({ (objCartDealItemsLocal) in
                        
                        var dictPizza = JSON()
                        dictPizza["prdname"] = JSON(objCartDealItemsLocal.product_name)
                        dictPizza["prdid"] = JSON(objCartDealItemsLocal.product_id)
                        dictPizza["qty"] = "1"
                        dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        dictPizza["prdingrd"] = JSON(objCartDealItemsLocal.ingr_name)
                        dictPizza["removetop"] = JSON(objCartDealItemsLocal.remove_ingr_name)
                        //                        dictPizza["pbase"] = JSON(objCartDealItemsLocal.product_baseType)
                        //                        dictPizza["psaurce"] = JSON(objCartDealItemsLocal.product_sauce_type)
                        //                        dictPizza["psize"] = JSON(objCartDealItemsLocal.pizzaSize)
                        dictPizza["pcomnt"] = JSON(objCartDealItemsLocal.product_description)
                        dictPizza["variant"] = JSON(objCartDealItemsLocal.addedVariant)
                        
                        print("Customized \(objCartDealItemsLocal.isCustomized)")
                        if objCartDealItemsLocal.isCustomized == "1" {
                            dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPriceForDeal(objCart: objCartDealItemsLocal)))
                        } else {
                            dictPizza["prdvalue"] = JSON(objCart.product_price)
                        }
                        arraySubData.append(dictPizza)
                        
                    })
                }
                dictPizza["dealname"] = JSON(arraySubData)
                arrayDealItems.append(dictPizza)
            }
            
        }
        
        //--Half half
        let arrayHalf = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "4"}
        if(arrayHalf.count > 0)
        {
            arrayHalf.forEach { (objCart) in
                
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.half_name)
                dictPizza["prdid"] = JSON(objCart.half_id)
                dictPizza["prdvalue"] = JSON(objCart.half_price)
                dictPizza["qty"] = "1"
                dictPizza["rmingr"] = ""
                dictPizza["pcomnt"] = JSON(objCart.half_extra_instruction)
                
                var arraySubData : [JSON] = []
                
                let arrayDealsSubItems = Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == objCart.id}
                if(arrayDealsSubItems.count > 0)
                {
                    
                    arrayDealsSubItems.forEach({ (objCartDealItemsLocal) in
                        
                        var dictPizza = JSON()
                        dictPizza["prdname"] = JSON(objCartDealItemsLocal.product_name)
                        dictPizza["prdid"] = JSON(objCartDealItemsLocal.product_id)
                        dictPizza["qty"] = "1"
                        dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        dictPizza["prdingrd"] = JSON(objCartDealItemsLocal.ingr_name)
                        dictPizza["removetop"] = JSON(objCartDealItemsLocal.remove_ingr_name)
                        
                        dictPizza["pcomnt"] = JSON(objCartDealItemsLocal.product_description)
                        dictPizza["variant"] = JSON(objCartDealItemsLocal.addedVariant)
                        if objCartDealItemsLocal.isCustomized == "1"
                        {
                            dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPriceForDeal(objCart: objCartDealItemsLocal)))
                        } else {
                            dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        }
                        arraySubData.append(dictPizza)
                        
                    })
                }
                dictPizza["halfandhalf"] = JSON(arraySubData)
                arrayHalfHalfItems.append(dictPizza)
            }
            
        }
        
        dictCheckoutJson["full"] = JSON(arrayFullPizzaItems)
        dictCheckoutJson["deal"] = JSON(arrayDealItems)
        dictCheckoutJson["half"] = JSON(arrayHalfHalfItems)
        
        print("checkout dictCheckoutJson - ",dictCheckoutJson)
        
        return dictCheckoutJson
    }
    
    func setUpPrice(objCart:Cart) -> Float
    {
        var price : Float = 0.0
        
        if(objCart.isTypePasta == "1")
        {
            price = Float(objCart.product_price) ?? 0.0
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            return price
        }
        
        /*let id = objCart.defaultTypeId
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                price = Float(objCart.half_price) ?? 0.0
            case "1":
                price = Float(objCart.product_price) ?? 0.0
            case "2":
                price = Float(objCart.product_small_price) ?? 0.0
            default:
                price =  Float(objCart.product_price) ?? 0.0
            }
            
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            
            return price
            
        }*/
        
        price = Float(objCart.finalPrice) ?? 0.0
        /*if(!objCart.addedTopingsPrice.isEmpty)
        {
            price = price + Float(objCart.addedTopingsPrice)!
        }*/
        print("price - ",price)
        return price
    }
    
    func setUpPriceForDeal(objCart:CartDealItems) -> Float
    {
        var price : Float = 0.0
        
        let id = objCart.defaultTypeId
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                price = Float(objCart.product_half_price) ?? 0.0
            case "1":
                price = Float(objCart.product_price) ?? 0.0
            case "2":
                price = Float(objCart.product_small_price) ?? 0.0
            default:
                price =  Float(objCart.product_price) ?? 0.0
            }
            
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            
            return price
            
        }
        return price
    }
    
    //MARK:- Deafults save
    func SaveUserDetailsToDefaults()
    {
        let UserObjects =  Array(globalRealm.objects(CheckoutUser.self))
        if(UserObjects.count > 0)
        {
//            UpdateUserData
            let objCheckOutUser = UserObjects[0]
            CheckoutUser().UpdateUserData(objUser: objUser, PKId: objCheckOutUser.id)
            
        }else{
            let PKId = checkoutUserIncrementID()
            let objCheckoutUser = CheckoutUser().setData(objUser: objUser, PKId: PKId)
            try! globalRealm.write
            {
                globalRealm.add(objCheckoutUser)
            }
        }

    }
    func SaveOrderDetailsToDefaults()
    {
        let OrderObjects =  Array(globalRealm.objects(CheckoutOrder.self))
        if(OrderObjects.count > 0)
        {
            let objCheckOutOrder = OrderObjects[0]
            CheckoutOrder().UpdateOrderData(objOrder: objOrder, PKId: objCheckOutOrder.id)
            
        }else{
            let PKId = checkoutOrderIncrementID()
            let objCheckoutOrder = CheckoutOrder().setData(objOrder: objOrder, PKId: PKId)
            try! globalRealm.write
            {
                globalRealm.add(objCheckoutOrder)
            }
        }       
        
    }
    
    //MARK:- Delete All Cart Item
    
    func deleteAllItemsfromCart() {
        let cartSubItemsObjects =  Array(globalRealm.objects(CartDealItems.self))
        if cartSubItemsObjects.count > 0
        {
            cartSubItemsObjects.forEach({ (objCartDealItems) in
                try! globalRealm.write {
                    globalRealm.delete(objCartDealItems)
                }
            })
        }
        
        let cart =  Array(globalRealm.objects(Cart.self))
        if cart.count > 0
        {
            cart.forEach({ (objCart) in
                try! globalRealm.write {
                    globalRealm.delete(objCart)
                }
            })
        }
    }
    
    //MARK:- Other mehods
    func attributedString(string1 : String , string2 : String,color1: UIColor,color2:UIColor,font1:UIFont,font2:UIFont) -> NSAttributedString?
    {
        let attributes1 = [
            NSAttributedString.Key.font : font1,
            NSAttributedString.Key.foregroundColor: color1
            ] as [NSAttributedString.Key : Any]
        
        let attributes2 = [
            NSAttributedString.Key.font : font2,
            NSAttributedString.Key.foregroundColor: color2
            ] as [NSAttributedString.Key : Any]
        
        
        let attributedString1 = NSAttributedString(string: "\(string1)", attributes: attributes1)
        let attributedString2 = NSAttributedString(string: " \(string2)", attributes: attributes2)
        
        let FormatedString = NSMutableAttributedString()
        
        FormatedString.append(attributedString1)
        FormatedString.append(attributedString2)
        ///
        return FormatedString
    }
    
}
extension UIViewController 
{    
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
    
    func navigateToViewcontroller(obj:UIViewController)  {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateUserWithLG(obj : UIViewController)
    {
        let navigationController = NavigationController(rootViewController: obj)
        
        let mainViewController = MainViewController()
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 6)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
    }
}
