//
//  Global.swift
//  PizzaApp
//
//  Created by Jaydeep on 19/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import RealmSwift
import SwiftyJSON


// store detaqil3

var dictStoreDetail = JSON()

// Delivery Type

var intDeliveryType = selectOrderType.pickup

// realm object

//var realm = try! Realm()
var globalRealm = try! Realm()

// Pizza Type

var arrPizzaType : [JSON] = setUpPizzaType()
var arrSauce : [String] = ["Tomato Sauce","No Sauce","BBQ Sauce","Garlic Sauce"]
var arrSubtype : [String] = ["Classic Base","Thin"]
var cvvNumber:Int = 3

// Device type

var strDeviceType = "1"
var strCurrenrcy = "$"
var strOrderPrefix = "M"

//MARK:- Model object

var objUser = User()
var objOrder = Order()

// Google Maps Key

let kGoogleMapsKey = "AIzaSyAI8Oi8hv4wq2YbAMZho9jC86wwzXlf4W0"

//naviagation bar height

var navHeight:CGFloat = 24

//Paypal
let kMerchantEmail = "pizza888cranbourne-facilitator@gmail.com"
let kBuyerEmail = "pizza888cranbourne-buyer@gmail.com"
let kPaypalSandboxClientId = "AVUWhRPv-dVHuKI_dvoilX_Q-XjimYkQf1L6KBewkkXyPHtDb65vI73JZcj_2MQ9wo-5LC-18uT7O-9y"

let kPaypalLiveClientId = "AcDhKQurXXO126b4q_otFShK8wFPdwVJAyiN7M8AytPTwKrciL4iHd73ex9xMzxDU-ZwpAe6nShPfaBY"
// navigation image

var navImage = (UIImage.init(named: "bg_header_round")?.resizableImage(withCapInsets: .zero, resizingMode: .stretch))!

// Storyboards
let objStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

// common string

let StringFilePath = Bundle.main.path(forResource: "CommonString", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)

// validation string

let validationFilePath = Bundle.main.path(forResource: "Validation", ofType: "plist")
let validationStrings = NSDictionary(contentsOfFile: validationFilePath!)

var dictGlobalCartData : [JSON] = []

var GlobalCartPrice : Float = 0.0

//MARK:- Navigationbar custom
let vwnav = Bundle.main.loadNibNamed("ViewNav", owner: nil, options: nil)?[0] as? ViewNav

//MARK: - UIScreen Bounds
var screenRect = CGRect()


//

//MARK:- Pagecontrol Color

var pageControlColor =  UIColor.lightGray.cgColor

//MARK:- PrintFont

func printFonts()
{
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        print("Font Names = [\(names)]")
    }
}

//
func getCommonString(key:String) -> String
{
    return dictStrings?.object(forKey: key) as? String ?? ""
}
//MARK:- Get Pizza Type
func setUpPizzaType() -> [JSON]
{
    var arrayPizzaTypeNames : [String] = ["Gluten Free","Large","Small"]
//    var arrayPizzaTypePrice : [String] = ["4.5","0.0","0.0"]

    var arrayPizzaType : [JSON] = []
    
    for i in 0..<arrayPizzaTypeNames.count
    {
        var json = JSON()
        json["typeId"].stringValue = "\(i)"
        json["typeName"].stringValue = arrayPizzaTypeNames[i]
//        json["typePrice"].stringValue = arrayPizzaTypePrice[i]
        arrayPizzaType.append(json)
    }
    
    return arrayPizzaType
}
func getPizzaTypeNames() -> [String]
{
    var arrayNames : [String] = []
    let arrayTypes = setUpPizzaType()
    arrayTypes.forEach { (json) in
        arrayNames.append(json["typeName"].stringValue)
    }
    return arrayNames
    
}
func getPastaBaseNames() -> [String]
{
    //    var arrayNames : [String] = []
    let arrayBase = ["Spaghetti","Fettuccine","Penne"]
    return arrayBase
    
}
func getPastaSizeNames() -> [String]
{
    
    let arrayBase = ["Large"]
    return arrayBase
    
}
func getPriceFormatedValue(strPrice : Float) -> String
{
    let str = "\(String(format: "%0.2f", strPrice))"
    return str
}
//get Validaion
func getValidationString(key:String) -> String
{
    return validationStrings?.object(forKey: key) as? String ?? ""
}

//MARK: - Toast
func makeToast(message : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = message
    messageSnack.duration = 5
    MDCSnackbarManager.show(messageSnack)
}

//MARK: - Date
func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
    }
    //print("convertedDate - ",convertedDate)
    return convertedDate
}
func DateToString(Formatter : String,date : Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    let convertedString = dateformatter.string(from: date)
    return convertedString
}
func StringToConvertedStringDate(strDate : String,strDateFormat : String,strRequiredFormat:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = strDateFormat
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = strRequiredFormat
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
    
}
func getFormatedDate(date: Date,Formate:String) -> String
{
    let currentTimeStamp = DateToString(Formatter: Formate, date: date)
    return currentTimeStamp
}
//MARK:- Timezone

var localTimeZoneName: String { return TimeZone.current.identifier }

//MARK:-  Loader Property

let Defaults = UserDefaults.standard
var strLoader:String = ""
var LoaderType:Int = 23
var Loadersize = CGSize(width: 30, height: 30)



func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = OrignalFormatter
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = YouWantFormatter
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
    
}
