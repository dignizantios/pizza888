//
//  VariantCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 01/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class VariantCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var btnSelectVariantOutlet: UIButton!
    
    //MARK:- ViewLife Cycle
    
    override func awakeFromNib() {
        [lblTypeName,lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
    }
    
}
