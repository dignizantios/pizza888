//
//  MenuCollectionViewCell.swift
//  PizzaApp
//
//  Created by Khushbu on 23/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgvwMenu : UIImageView!
    @IBOutlet var btnDownload : UIButton!
    @IBOutlet var lblStoreName : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
