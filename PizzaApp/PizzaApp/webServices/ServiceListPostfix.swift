//
//  ServiceListPostfix.swift
//  Hand2Home
//
//  Created by YASH on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

//MARK: - UserName /Password

let basic_username = "root"

let basic_password = "123"

//let kBasicURL = "http://dignizant.com/Pizza-Api/api/pizza888_api/"

//let kBasicURL = "http://tech88.com.au/Pizza-Api/api/pizza888_api/"

let kBasicURL = "https://tech88.com.au/dignizant/Pizza-Api/api/pizza888_api/"

let DeviceType = "1"


//MARK:- Response flag

var strSuccessResponse = "1"
var strAccessDenid = "-1"
var strLang = "0"

//MARK:- API

var kLogin = "login"

var kRegi = "user_register"

var kGetBannerList = "website_banner"

var kForgotPassword = "forgot_password"

var kChangePassword = "change_password"

var kSaveUserProfile = "save_user_profile"

var kUserLogout = "user_logout"

var kStoreOpenClose = "store_open_close"

var kGetTimes = "gettimes"

var kGetCategoriesProduct = "product_categories_list"

var kTradingHours = "trading_hours"

var kSuburbList = "select_store_suburb"

var kStoreList = "select_store"

var kGetProduct = "productlist"

var kProductCustomize = "product_customize"

var kOrderPlace = "checkout"

var kContactUs = "contact_us"

var kGetMenu = "menu_store"

var kGetPolicy = "privacy_policy"

var kCheckout_cash = "checkout_cash"

var kCheckCouponCode = "check_couponcode"

var kTrackOrder = "order_track"

var kOrderHistory = "order_history"

var kGetCrediCardDetail = "getpaymentelement"

var kPayWithCreditCard = "paymcreditcard"

var kPayWithPaypal = "paypal"
