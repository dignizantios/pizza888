//
//  Cart.swift
//  PizzaApp
//
//  Created by Jaydeep on 10/01/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class Cart: Object {
    
    
    @objc dynamic var id = 0
    @objc dynamic var product_id = 0
    @objc dynamic var product_name = ""
    @objc dynamic var product_price = ""
    @objc dynamic var product_category = ""

    @objc dynamic var product_half_price = ""
    @objc dynamic var product_small_price   = ""
    @objc dynamic var remove_ingr_id   = ""
    @objc dynamic var remove_ingr_name   = ""
    @objc dynamic var ingr_add   = ""
    @objc dynamic var ingr_name   = ""
    @objc dynamic var pizzaSize = ""
    @objc dynamic var defaultTypeId   = ""
    @objc dynamic var product_description = ""
    @objc dynamic var product_quanitity = ""
    @objc dynamic var product_type = ""
    @objc dynamic var product_sauce_type = ""
    @objc dynamic var product_baseType = ""
    @objc dynamic var product_extra_description = ""
    @objc dynamic var addedTopingsPrice = ""
    @objc dynamic var specification = ""
    @objc dynamic var isCustomized = ""
    @objc dynamic var isTypePasta = ""

    @objc dynamic var deal_id = 0
    @objc dynamic var deal_name = ""
    @objc dynamic var deal_description = ""
    @objc dynamic var deal_price = ""
    @objc dynamic var deal_extra_instruction = ""

    @objc dynamic var half_id = 0
    @objc dynamic var half_name = ""
    @objc dynamic var half_description = ""
    @objc dynamic var half_price = ""
    @objc dynamic var half_extra_instruction = ""
    
    @objc dynamic var finalPrice = ""
    @objc dynamic var addedVariant = ""
//    @objc dynamic var quanitity = ""
     @objc dynamic var quantity = ""
    @objc dynamic var category_name = ""
    
    //MARK:- Set Primary key
    override class func primaryKey() -> String {
        return "id"
    }
    
    func storeData(dict:JSON,PKId:Int) -> Cart {
        
        /*
         "baseType" : "Thin",
         "half_price" : "14.50",
         "small_price" : "11.50",
         "remove_ingr_name" : "Chicken,Spanish Onions",
         "specification" : "Chicken, spinach, tomatoes, Spanish Onions, kalamata olives and feta cheese",
         "price" : "18.00",
         "defaultTypeId" : "2",
         "is_customized" : "0",
         "product_name" : "Greek",
         "ingr_add" : "2,3",
         "extra_description" : "",
         "remove_ingr_id" : "8,32",
         "pizzaSize" : "Small",
         "addedTopingsPrice" : "4.0",
         "product_id" : "126",
         "type" : "2",
         "sauce_type" : "BBQ Sauce",
         "ingr_name" : "Rasher Bacon,Virginia Ham",
         "description" : "Less spicy pizza"
         
         
         @objc dynamic var deal_id = 0
         @objc dynamic var deal_name = ""
         @objc dynamic var deal_description = ""
         @objc dynamic var deal_price = ""
         @objc dynamic var deal_customized_addedPrice = ""
        */
        
        
        let objCart = Cart()
        objCart.id = PKId
        objCart.product_id = dict["product_id"].intValue
        objCart.product_category = dict["category_name"].stringValue
        objCart.product_name = dict["product_name"].stringValue
        objCart.product_baseType = dict["baseType"].stringValue
        objCart.product_half_price = dict["half_price"].stringValue
        objCart.product_small_price = dict["small_price"].stringValue
        objCart.remove_ingr_name = dict["remove_ingr_name"].stringValue
        objCart.specification = dict["specification"].stringValue
//        objCart.product_price = dict["price"].stringValue
        objCart.product_price = dict["product_price"].stringValue
        objCart.defaultTypeId = dict["defaultTypeId"].stringValue
        
        objCart.ingr_add = dict["ingr_add"].stringValue
        objCart.product_extra_description = dict["extra_description"].stringValue
        objCart.remove_ingr_id = dict["remove_ingr_id"].stringValue
        objCart.pizzaSize = dict["pizzaSize"].stringValue
        objCart.addedTopingsPrice = dict["addedTopingsPrice"].stringValue
        objCart.product_type = dict["type"].stringValue
        objCart.product_sauce_type = dict["sauce_type"].stringValue
        objCart.ingr_name = dict["ingr_name"].stringValue
        objCart.product_description = dict["description"].stringValue
        objCart.isCustomized = dict["customized"].stringValue
        objCart.isTypePasta = dict["isTypePasta"].stringValue
        
        objCart.deal_id = dict["dealid"].intValue
        objCart.deal_name = dict["dealName"].stringValue
        objCart.deal_description = dict["dealdiscription"].stringValue
        objCart.deal_price = dict["dealPrice"].stringValue
        objCart.deal_extra_instruction = dict["deal_extra_instruction"].stringValue
        
        objCart.half_id = dict["half_id"].intValue
        objCart.half_name = dict["halfName"].stringValue
        objCart.half_description = dict["halfdiscription"].stringValue
        objCart.half_price = dict["halfPrice"].stringValue
        objCart.half_extra_instruction = dict["half_extra_instruction"].stringValue
        
        objCart.finalPrice = dict["finalPrice"].stringValue
        objCart.addedVariant = dict["addedVariant"].stringValue
        objCart.quantity = dict["quantity"].stringValue
        objCart.category_name = dict["category_name"].stringValue
        return objCart
    }
   
    /*func storeDealData(dict:JSON,PKId:Int) -> Cart {
        
        
        /*
         {
         "dealName": "Deal 1",
         "type": "3",
         "dealid": "10",
         "dealdiscription": "2 Small Traditional Pizza, 1 Drink 1.25 Ltr and 1 Garlic Bread",
         "big_img": "1449284642-pizza.jpg",
         "dealPrice": "19.90"
         }
 
        */
        
        let objCart = Cart()
        objCart.id = PKId
        objCart.deal_id = dict["dealid"].intValue
        objCart.product_type = dict["type"].stringValue
        objCart.deal_name = dict["dealid"].stringValue
        objCart.deal_description = dict["dealdiscription"].stringValue
        objCart.deal_price = dict["dealPrice"].stringValue
        
        return objCart
    }
    */
    func GetData() -> JSON
    {
        var dict = JSON()
        
        dict["cart_id"].intValue = self.id
        dict["product_id"].intValue = self.product_id
        dict["category_name"].stringValue = self.product_category
        dict["product_name"].stringValue = self.product_name
        dict["baseType"].stringValue = self.product_baseType
        dict["half_price"].stringValue = self.product_half_price
        dict["small_price"].stringValue = self.product_small_price
        dict["remove_ingr_name"].stringValue = self.remove_ingr_name
        dict["specification"].stringValue = self.specification
//        dict["price"].stringValue = self.product_price
        dict["product_price"].stringValue = self.product_price
        dict["defaultTypeId"].stringValue = self.defaultTypeId
        
        dict["ingr_add"].stringValue = self.ingr_add
        dict["extra_description"].stringValue = self.product_extra_description
        dict["remove_ingr_id"].stringValue = self.remove_ingr_id
        dict["pizzaSize"].stringValue = self.pizzaSize
        dict["addedTopingsPrice"].stringValue = self.addedTopingsPrice
        dict["type"].stringValue = self.product_type
        dict["sauce_type"].stringValue = self.product_sauce_type
        dict["ingr_name"].stringValue = self.ingr_name
        dict["description"].stringValue = self.product_description
        dict["customized"].stringValue = self.isCustomized
        dict["isTypePasta"].stringValue = self.isTypePasta

        dict["dealid"].intValue = self.deal_id
        dict["dealName"].stringValue = self.deal_name
        dict["dealdiscription"].stringValue = self.deal_description
        dict["dealPrice"].stringValue = self.deal_price
        dict["deal_extra_instruction"].stringValue = self.deal_extra_instruction
        
        dict["half_id"].intValue = self.half_id
        dict["halfName"].stringValue = self.half_name
        dict["halfdiscription"].stringValue = self.half_description
        dict["halfPrice"].stringValue = self.half_price
        dict["half_extra_instruction"].stringValue = self.half_extra_instruction
        
        dict["finalPrice"].stringValue = self.finalPrice
        dict["addedVariant"].stringValue = self.addedVariant
        dict["quantity"].stringValue = self.quantity
        dict["category_name"].stringValue = self.category_name
        return dict
        
        
    }
    func storeHalfHalfData(dict:JSON,PKId:Int) -> Cart {
        
        
        let objCart = Cart()
        objCart.id = PKId
        objCart.product_type = dict["type"].stringValue
        objCart.half_id = dict["half_id"].intValue
        objCart.half_name = dict["halfName"].stringValue
        objCart.half_description = dict["halfdiscription"].stringValue
        objCart.half_price = dict["halfPrice"].stringValue
        objCart.half_extra_instruction = dict["half_extra_instruction"].stringValue
        objCart.category_name = dict["category_name"].stringValue
        
        return objCart
    }
}
