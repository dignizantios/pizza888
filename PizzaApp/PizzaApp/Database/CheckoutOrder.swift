//
//  CheckoutOrder.swift
//  PizzaApp
//
//  Created by Khushbu on 28/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class CheckoutOrder: Object
{
    @objc dynamic var id = 0
    
    //MARK:- Future Order Date Time
    
    @objc dynamic  var strFutureOrderDate : String = ""
    @objc dynamic  var strFutureOrderTime : String = ""
    
    //MARK:- Apply Coupon Code
    @objc dynamic  var strCouponCode : String = ""
    
    //MARK:- Confirm Order
    @objc dynamic  var isSelectPaymentOption : Bool = false
    @objc dynamic  var strSelectPaymentOption : String = ""
    
    //MARK:- Select Store
    @objc dynamic  var strSelectStore : String = ""
    @objc dynamic  var strOrderNumber : String = ""
    @objc dynamic  var strComment : String = ""
    @objc dynamic var strStorePhoneNumber : String = ""
    @objc dynamic var strStoreAddress : String = ""
    
    @objc dynamic var strSelectedOrderType = selectOrderType.pickup.rawValue
    @objc dynamic var strSelectedPickUpTime = selectedPickupTime.asap.rawValue
    @objc dynamic var strSelectedStoreId : String = ""
    
    //MARK:- Set Primary key
    override class func primaryKey() -> String {
        return "id"
    }
    
    func setData(objOrder : Order,PKId:Int) -> CheckoutOrder
    {
        
        let objCheckout = CheckoutOrder()
        objCheckout.id = PKId
        objCheckout.strFutureOrderDate = objOrder.strFutureOrderDate
        objCheckout.strFutureOrderTime = objOrder.strFutureOrderTime
        objCheckout.strCouponCode = objOrder.strCouponCode
        objCheckout.isSelectPaymentOption = objOrder.isSelectPaymentOption
        objCheckout.strSelectPaymentOption = objOrder.strSelectPaymentOption
        objCheckout.strSelectStore = objOrder.strSelectStore
        objCheckout.strOrderNumber = objOrder.strOrderNumber
        objCheckout.strComment = objOrder.strComment
        objCheckout.strSelectedOrderType = objOrder.strSelectedOrderType.rawValue
        objCheckout.strSelectedPickUpTime = objOrder.strSelectedPickUpTime.rawValue
        objCheckout.strSelectedStoreId = objOrder.strSelectedStoreId
        objCheckout.strStoreAddress = objOrder.strStoreAddress
        objCheckout.strStorePhoneNumber = objOrder.strStorePhoneNumber
        return objCheckout
    }
    func UpdateOrderData(objOrder : Order,PKId:Int)
    {

        printUpdateData(objOrder : objOrder)
        let checkedOrder = Array(globalRealm.objects(CheckoutOrder.self)).filter { $0.id == PKId}
        if checkedOrder.count != 0
        {
            let objFirst  = checkedOrder.first
            try? globalRealm.write
            {
                objFirst?.strFutureOrderDate = objOrder.strFutureOrderDate
                objFirst?.strFutureOrderTime = objOrder.strFutureOrderTime
                objFirst?.strCouponCode = objOrder.strCouponCode
                objFirst?.isSelectPaymentOption = objOrder.isSelectPaymentOption
                objFirst?.strSelectPaymentOption = objOrder.strSelectPaymentOption
                objFirst?.strSelectStore = objOrder.strSelectStore
                objFirst?.strOrderNumber = objOrder.strOrderNumber
                objFirst?.strComment = objOrder.strComment
                objFirst?.strSelectedOrderType = objOrder.strSelectedOrderType.rawValue
                objFirst?.strSelectedPickUpTime = objOrder.strSelectedPickUpTime.rawValue
                objFirst?.strSelectedStoreId = objOrder.strSelectedStoreId
                objFirst?.strStoreAddress = objOrder.strStoreAddress
                objFirst?.strStorePhoneNumber = objOrder.strStorePhoneNumber
                globalRealm.add(objFirst!, update: .modified)
//                globalRealm.add(objFirst!, update: true)
            }
        }
    }
    func printUpdateData(objOrder : Order)
    {
        print("strFutureOrderDate - ",objOrder.strFutureOrderDate)
        print("strFutureOrderTime - ",objOrder.strFutureOrderTime)
        print("strCouponCode - ",objOrder.strCouponCode)
        print("isSelectPaymentOption - ",objOrder.isSelectPaymentOption)
        print("strSelectPaymentOption - ",objOrder.strSelectPaymentOption)
        print("strSelectStore - ",objOrder.strSelectStore)
        print("strOrderNumber - ",objOrder.strOrderNumber)
        print("strSelectedOrderType.rawValue - ",objOrder.strSelectedOrderType.rawValue)
        print("strSelectedPickUpTime.rawValue - ",objOrder.strSelectedPickUpTime.rawValue)
        print("strSelectedStoreId - ",objOrder.strSelectedStoreId)
        print("strStorePhoneNumber - ",objOrder.strStorePhoneNumber)
        print("strStoreAddress - ",objOrder.strStoreAddress)
    }
}
