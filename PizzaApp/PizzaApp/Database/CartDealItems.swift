//
//  CartDealItems.swift
//  PizzaApp
//
//  Created by Khushbu on 21/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class CartDealItems: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var cart_id = 0
    @objc dynamic var product_id = 0
    @objc dynamic var product_name = ""
    @objc dynamic var product_price = ""
    @objc dynamic var product_half_price = ""
    @objc dynamic var product_small_price   = ""
    @objc dynamic var remove_ingr_id   = ""
    @objc dynamic var remove_ingr_name   = ""
    @objc dynamic var ingr_add   = ""
    @objc dynamic var ingr_name   = ""
    @objc dynamic var pizzaSize = ""
    @objc dynamic var defaultTypeId   = ""
    @objc dynamic var product_description = ""
    @objc dynamic var product_quanitity = ""
    @objc dynamic var product_type = ""
    @objc dynamic var product_sauce_type = ""
    @objc dynamic var product_baseType = ""
    @objc dynamic var product_extra_description = ""
    @objc dynamic var addedTopingsPrice = ""
    @objc dynamic var specification = ""
    @objc dynamic var isCustomized = ""
    @objc dynamic var addedVariant = ""
    @objc dynamic var category_name = ""
    
    
    //MARK:- Set Primary key
    override class func primaryKey() -> String {
        return "id"
    }
    
    func storeData(dict:JSON,PKId:Int,cartId:Int) -> CartDealItems {
        
        print("Deal item - ",dict)
        
        let objCartItem = CartDealItems()
        objCartItem.id = PKId
        objCartItem.cart_id = cartId
        
        objCartItem.product_id = dict["product_id"].intValue
        objCartItem.product_name = dict["product_name"].stringValue
        objCartItem.product_baseType = dict["baseType"].stringValue
        objCartItem.product_half_price = dict["half_price"].stringValue
        objCartItem.product_small_price = dict["small_price"].stringValue
        objCartItem.remove_ingr_name = dict["remove_ingr_name"].stringValue
        objCartItem.specification = dict["specification"].stringValue
        objCartItem.product_price = dict["price"].stringValue
        objCartItem.defaultTypeId = dict["defaultTypeId"].stringValue
        
        objCartItem.ingr_add = dict["ingr_add"].stringValue
        objCartItem.product_extra_description = dict["extra_description"].stringValue
        objCartItem.remove_ingr_id = dict["remove_ingr_id"].stringValue
        objCartItem.pizzaSize = dict["pizzaSize"].stringValue
        objCartItem.addedTopingsPrice = dict["addedTopingsPrice"].stringValue
        objCartItem.product_type = dict["type"].stringValue
        objCartItem.product_sauce_type = dict["sauce_type"].stringValue
        objCartItem.ingr_name = dict["ingr_name"].stringValue
        objCartItem.product_description = dict["description"].stringValue
        objCartItem.isCustomized = dict["customized"].stringValue
        objCartItem.addedVariant = dict["addedVariant"].stringValue
        objCartItem.category_name = dict["category_name"].stringValue

        return objCartItem
    }
    
    
}
