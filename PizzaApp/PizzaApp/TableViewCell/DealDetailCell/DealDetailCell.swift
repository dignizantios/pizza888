//
//  DealDetailCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 04/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class DealDetailCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var btnAddDealOutlet: UIButton!
    @IBOutlet weak var btnCustomizeOutlet: UIButton!
    @IBOutlet weak var viewOfCustomize: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblItemPrice: UILabel!
    
    
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [btnAddDealOutlet,btnCustomizeOutlet].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeRedColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        btnAddDealOutlet.setTitle(getCommonString(key: "Add_to_deal_key").uppercased(), for: .normal)
        btnCustomizeOutlet.setTitle(getCommonString(key: "Customize_key").uppercased(), for: .normal)
        
        imgProduct.layer.cornerRadius = imgProduct.frame.size.height/2
        imgProduct.layer.masksToBounds = true
        
        [lblItemPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
