//
//  CheckOutCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
class CheckOutCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemDescription: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblSubInformation: UILabel!

    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var btnMinusOutlet: UIButton!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    //MARK:- Tablview life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblItemPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [lblTotalPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [lblItemDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblQuantity].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        [lblSubInformation].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpData(dict:JSON)
    {
        print("dict - ",dict)
        var strProductName = ""
        var strOtherInfo = ""

        if(dict["type"].stringValue == "2")
        {
            
            if(dict["customized"].stringValue == "1")
            {
                lblItemDescription.isHidden = false
                lblSubInformation.isHidden = false
                /*if(dict["isTypePasta"].stringValue == "1")
                {
                    strProductName = dict["product_name"].stringValue

                }else
                {
                    strProductName = dict["product_name"].stringValue + "(\(dict["pizzaSize"].stringValue))"
                }*/
                
                if(dict["pizzaSize"].stringValue != "")
                {
                    if(dict["isTypePasta"].stringValue == "1")
                    {
                        strProductName = dict["product_name"].stringValue
                        
                    }else
                    {
                        strProductName = dict["product_name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"
                    }
                    
                    
                }else{
                    strProductName = dict["product_name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"
                }
                

            }else if(dict["customized"].stringValue == "0")
            {
                lblItemDescription.isHidden = true
                lblSubInformation.isHidden = true
                
                if(dict["pizzaSize"].stringValue != "")
                {
                    if(dict["isTypePasta"].stringValue == "1")
                    {
                        strProductName = dict["product_name"].stringValue
                        
                    }else
                    {
                        strProductName = dict["product_name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"
                    }
                    
                    
                }else{
                    strProductName = dict["product_name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"
                }
            
            }
            
            
            setUpPrice(dict:dict)
            
            lblItemName.text = strProductName
            if(!dict["addedTopingsPrice"].stringValue.isEmpty)
            {
                let addedTopings = dict["ingr_name"].stringValue.split(separator: ",")
                if(addedTopings.count>0)
                {
                    addedTopings.forEach { (str) in
                        strOtherInfo = strOtherInfo + "\n+ \(str)"
                    }
                }
                let removeTopings = dict["remove_ingr_name"].stringValue.split(separator: ",")
                if(removeTopings.count>0)
                {
                    removeTopings.forEach { (str) in
                        strOtherInfo = strOtherInfo + "\n- \(str)"
                    }
                }
            }
            /*if(!dict["baseType"].stringValue.isEmpty && dict["isTypePasta"].stringValue == "0")
            {
                strOtherInfo = strOtherInfo + "\nBase : \(dict["baseType"].stringValue)"
            }
            if(!dict["sauce_type"].stringValue.isEmpty && dict["isTypePasta"].stringValue == "0")
            {
                strOtherInfo = strOtherInfo + "\nSauce : \(dict["sauce_type"].stringValue)"
            }*/
//            strOtherInfo = strOtherInfo + "\n \(dict["addedVariant"].stringValue)"
            
            if(!dict["description"].stringValue.isEmpty && dict["isTypePasta"].stringValue == "0")
            {
                lblItemDescription.text = "Comment : \(dict["description"].stringValue)"
                lblItemDescription.isHidden = false
            }
            else{
                lblItemDescription.isHidden = true
            }
            
            lblSubInformation.text = strOtherInfo
            
        }
        else if(dict["type"].stringValue == "3") //Deal
        {
            
            /*
             objCart.deal_id = dict["dealid"].intValue
             objCart.deal_name = dict["dealName"].stringValue
             objCart.deal_description = dict["dealdiscription"].stringValue
             objCart.deal_price = dict["dealPrice"].stringValue
 
            */
            
            lblItemName.text = "\(dict["category_name"].stringValue) \n\(dict["dealName"].stringValue)"
//            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["dealPrice"].floatValue))"
            
            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["dealPrice"].floatValue)) X 1"
           
            
            lblTotalPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["dealPrice"].floatValue))"
            

            var strInformation = ""
            let arraySubdata = dict["sub_data"].arrayValue
            
            arraySubdata.forEach { (dictSub) in
                
                if(dictSub["type"].stringValue == "1")
                {
                    strInformation = strInformation + dictSub["product_name"].stringValue
                    strInformation =  strInformation + "\n------------------------------------\n"
                }
                else if(dictSub["type"].stringValue == "2" || dictSub["type"].stringValue == "3")
                {
                    strInformation = strInformation + SubInformationText(dict:dictSub)
                    strInformation = strInformation + "\n------------------------------------\n"
                }
                
            }
            lblSubInformation.text = strInformation
            lblSubInformation.isHidden = false

            
            lblItemDescription.text = ""
            lblItemDescription.isHidden = true
            
        }
        else if(dict["type"].stringValue == "4")
        {
            lblItemName.text = "\(dict["category_name"].stringValue) \n\(dict["halfName"].stringValue)"
//            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["half_price"].floatValue))"
            
            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["half_price"].floatValue)) X 1"
            
            
            lblTotalPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["half_price"].floatValue))"
            
            var strInformation = ""
            let arraySubdata = dict["sub_data"].arrayValue
            
            arraySubdata.forEach { (dictSub) in
                
                if(dictSub["type"].stringValue == "1")
                {
                    strInformation = strInformation + dictSub["product_name"].stringValue
                    strInformation = strInformation + "\n------------------------------------\n"
                }
                else if(dictSub["type"].stringValue == "2")
                {
                    strInformation = strInformation + SubInformationText(dict:dictSub)
                    strInformation = strInformation + "\n------------------------------------\n"
                    
                }
                
            }
            lblSubInformation.text = strInformation
            lblSubInformation.isHidden = false
            
            
            lblItemDescription.text = ""
            lblItemDescription.isHidden = true
        }
        else{
            
            lblItemDescription.isHidden = true
            lblSubInformation.isHidden = true
            if(dict["specification"].stringValue != "")
            {
//                lblItemName.text = dict["product_name"].stringValue + "\n(\(dict["specification"].stringValue))"
                lblItemName.text = dict["product_name"].stringValue
            }else{
                lblItemName.text = dict["product_name"].stringValue
            }
            
            
            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["finalPrice"].floatValue)) X \(dict["quantity"].stringValue)"
            
            let finalGrandPrice = dict["finalPrice"].floatValue * dict["quantity"].floatValue
            
            lblTotalPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: finalGrandPrice))"
            
        }
        
    }
    func SubInformationText(dict:JSON) -> String
    {
        var strProductName = ""
        
        if(dict["customized"].stringValue == "0")
        {
            strProductName = dict["product_name"].stringValue
            
        }else{
            strProductName = dict["product_name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"
            if(!dict["addedTopingsPrice"].stringValue.isEmpty)
            {
                let addedTopings = dict["ingr_name"].stringValue.split(separator: ",")
                if(addedTopings.count>0)
                {
                    addedTopings.forEach { (str) in
                        strProductName = strProductName + "\n  + \(str)"
                    }
                }
                let removeTopings = dict["remove_ingr_name"].stringValue.split(separator: ",")
                if(removeTopings.count>0)
                {
                    removeTopings.forEach { (str) in
                        strProductName = strProductName + "\n  - \(str)"
                    }
                }
            }
            if(!dict["baseType"].stringValue.isEmpty)
            {
                strProductName = strProductName + "\n  Base : \(dict["baseType"].stringValue)"
            }
            if(!dict["sauce_type"].stringValue.isEmpty)
            {
                strProductName = strProductName + "\n  Sauce : \(dict["sauce_type"].stringValue)"
            }
            
            if(!dict["description"].stringValue.isEmpty)
            {
                strProductName = strProductName + "\n  Comment : \(dict["description"].stringValue)"
            }
        }
        
        
        return strProductName
    }
    func setUpPrice(dict:JSON)
    {
        var price : Float = 0.0

        if(dict["isTypePasta"].stringValue == "1")
        {
            price = dict["finalPrice"].floatValue
            /*if(!dict["addedTopingsPrice"].stringValue.isEmpty)
            {
                price = price + dict["addedTopingsPrice"].floatValue
            }
            lblItemPrice.text = "\(strCurrenrcy) \(price)"*/
            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["finalPrice"].floatValue)) X \(dict["quantity"].stringValue)"
            
            let finalGrandPrice = dict["finalPrice"].floatValue * dict["quantity"].floatValue
            
            lblTotalPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: finalGrandPrice))"
            return
        }
        
        /*let id = dict["defaultTypeId"].stringValue
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                price = dict["half_price"].floatValue
            case "1":
                price = dict["price"].floatValue
            case "2":
                price = dict["small_price"].floatValue
            default:
                price = dict["price"].floatValue
            }
            
            if(!dict["addedTopingsPrice"].stringValue.isEmpty)
            {
                price = price + dict["addedTopingsPrice"].floatValue
            }
            
            lblItemPrice.text = "\(strCurrenrcy) \(price)"
        
        }*/
//        lblItemPrice.text = "\(strCurrenrcy) \(dict["finalPrice"].stringValue)"
        lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["finalPrice"].floatValue)) X \(dict["quantity"].stringValue)"
        
        let finalGrandPrice = dict["finalPrice"].floatValue * dict["quantity"].floatValue
        
        lblTotalPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: finalGrandPrice))"
    }
    
}
