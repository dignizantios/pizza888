//
//  MyAccountCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyAccountCell: UITableViewCell {
    
    //MARK:- outlet Zone
    @IBOutlet weak var vwUpperSep: UIView!
    @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    //MARK:- Tableview life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblName].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.black
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Setupdata
    
    func setData(_ dict:JSON)  {
        
        lblName.text = dict["name"].stringValue
        imgOfUser.image = UIImage(named: dict["image"].stringValue)
    }

}
