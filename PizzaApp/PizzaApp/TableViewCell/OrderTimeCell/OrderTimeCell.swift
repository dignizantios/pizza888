//
//  OrderTimeCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit

class OrderTimeCell: UITableViewCell {

    //MARK:- Outlet Zone
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblTime].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.black
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
