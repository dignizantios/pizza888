//
//  SelectItemTableCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SkeletonView
import SwiftyJSON

class SelectItemTableCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwOfBottom: UIView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemDescription: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var vwCustomize: CustomView!
    @IBOutlet weak var lblCustomizeType: UILabel!
    @IBOutlet weak var btnCustomizedOutlet: UIButton!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var btnMinusOutlet: UIButton!
    @IBOutlet weak var btnSelectTypeOutlet: UIButton!
    @IBOutlet weak var btnCustomizeSelectOutlet: UIButton!
    @IBOutlet weak var vwType: UIView!
    @IBOutlet weak var vwQuantity: UIView!
    @IBOutlet weak var btnAddProductOutlet: UIButton!
    
    @IBOutlet weak var lblBase: UILabel!
    @IBOutlet weak var btnSelectBaseOutlet: UIButton!
    @IBOutlet weak var vwSelectBase: UIView!

    @IBOutlet weak var vwHidden: UIView!

    @IBOutlet weak var vwCustomizeContent: UIView!
    @IBOutlet weak var imgProduct: UIImageView!

    
    //MARK:- Tablview life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblItemPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblItemDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [lblType,lblQuantity].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblCustomizeType].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblItemName,lblItemDescription].forEach { (lbl) in
            lbl?.showAnimatedSkeleton()
        }
//        vwOfBottom.showAnimatedSkeleton()
        
        
        imgProduct.layer.cornerRadius = imgProduct.frame.size.height/2
        imgProduct.layer.masksToBounds = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func stopAnimationSkeleton()
    {
        [lblItemName,lblItemPrice,lblItemDescription,lblQuantity,lblCustomizeType,lblType].forEach { (lbl) in
            lbl?.hideSkeleton()
        }
        
        vwOfBottom.hideSkeleton()
        
    }
    
    func setData(dict:JSON)
    {
        lblItemName.text = dict["product_name"].stringValue
        lblItemDescription.text = dict["specification"].stringValue
        lblQuantity.text = dict["quantity"].stringValue
        /*if dict["is_customized"].stringValue == "1"
        {
            vwCustomize.borderColor = UIColor.appThemeGreenColor
            lblCustomizeType.textColor  = UIColor.appThemeGreenColor
            btnCustomizedOutlet.isSelected = true
            lblCustomizeType.text = getCommonString(key: "Customize_key")
        }
        else
        {
            vwCustomize.borderColor = UIColor.appThemeRedColor
            lblCustomizeType.textColor  = UIColor.appThemeRedColor
            btnCustomizedOutlet.isSelected = false
            lblCustomizeType.text = "\(getCommonString(key: "Customize_key"))d"
        }*/
        vwCustomize.borderColor = UIColor.appThemeRedColor
        lblCustomizeType.textColor  = UIColor.appThemeRedColor
        btnCustomizedOutlet.isSelected = false
        lblCustomizeType.text = "\(getCommonString(key: "Customize_key"))"
        
        setUpPrice(dict:dict)
        
    }
    
    func setUpPrice(dict:JSON)
    {
//        print("price setup dict - ",dict)
        lblItemPrice.text = "\(strCurrenrcy) \(dict["product_price"].stringValue)"
        
        /*let id = dict["defaultTypeId"].stringValue
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                lblType.text = json["typeName"].stringValue
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                lblItemPrice.text = "\(strCurrenrcy) \(dict["half_price"].stringValue)"
            case "1":
                lblItemPrice.text = "\(strCurrenrcy) \(dict["price"].stringValue)"
            case "2":
                lblItemPrice.text = "\(strCurrenrcy) \(dict["small_price"].stringValue)"
            default:
                lblItemPrice.text = "\(strCurrenrcy) \(dict["price"].stringValue)"
            }
        }*/
    }

}
