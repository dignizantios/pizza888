//
//  OrderStatusCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderStatusCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwUpper: UIView!
    @IBOutlet weak var vwRound: CustomView!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var vcLower: UIView!
    @IBOutlet weak var btnStatus: UIButton!
    
    //MARK:- Tableview life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [btnStatus].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        }
        
        btnStatus.setTitleColor(UIColor.appThemeGreenColor, for: .selected)
        btnStatus.setTitleColor(UIColor.appThemeLightGrayColor, for: .normal)
        btnStatus.tintColor = UIColor.clear
        btnStatus.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Setupdata
    
    func setData(_ dict:JSON)  {
        btnStatus.setTitle(dict["name"].stringValue, for: .normal)
        if dict["is_selected"].stringValue == "1"{
            btnStatus.isSelected = true
            [vwRound,vwUpper,vcLower].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeGreenColor
            }
        } else {
            btnStatus.isSelected = false
            [vwRound,vwUpper,vcLower].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeLightGrayColor
            }
        }
        if dict["is_uppar_selected"].stringValue == "1"{
            [vwUpper].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeGreenColor
            }
        } else{
            [vwUpper].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeLightGrayColor
            }
        }
        
        if dict["is_lower_selected"].stringValue == "1"{
            [vcLower].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeGreenColor
            }
        } else{
            [vcLower].forEach { (view) in
                view?.backgroundColor = UIColor.appThemeLightGrayColor
            }
        }
    }

}
