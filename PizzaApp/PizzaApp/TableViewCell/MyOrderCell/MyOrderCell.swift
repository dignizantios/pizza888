//
//  MyOrderCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SkeletonView
import SwiftyJSON

class MyOrderCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblOrderDateTitle: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderPaymentStatusTitle: UILabel!
    @IBOutlet weak var lblOrderPaymentStatus: UILabel!
    @IBOutlet weak var lblOrderFulfillStatusTitle: UILabel!
    @IBOutlet weak var lblOrderFulfillStatus: UILabel!
    @IBOutlet weak var lblOrderTotalTitle: UILabel!
    @IBOutlet weak var lblOrderTotal: UILabel!
   
    @IBOutlet weak var btnReorderOutlet: UIButton!
    @IBOutlet weak var btnOrderStatusOutlet: UIButton!
    @IBOutlet weak var lblOrderDelivryDateTitle: UILabel!
    @IBOutlet weak var vwDeleiveryDate: UIView!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    //MARK:- TableView cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblOrderNumber].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.black
        }
        
        [lblStoreTitle,lblOrderDateTitle,lblOrderPaymentStatusTitle,lblOrderFulfillStatusTitle,lblOrderTotalTitle,lblOrderDelivryDateTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [lblStoreName,lblOrderDate,lblOrderFulfillStatus,lblOrderPaymentStatus,lblOrderTotal,lblDeliveryDate].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [btnReorderOutlet,btnOrderStatusOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .medium)
            btn?.setTitleColor(UIColor.appThemeRedColor, for: .normal)
        }
        
        lblStoreTitle.text = getCommonString(key: "From_key") + " : "
        lblOrderDateTitle.text = getCommonString(key: "Order_date_key").capitalized + " : "
        lblOrderPaymentStatusTitle.text = getCommonString(key: "Payment_status_key").capitalized + " : "
        lblOrderFulfillStatusTitle.text = getCommonString(key: "Fullfillment_status_key").capitalized + " : "
        lblOrderTotalTitle.text = getCommonString(key: "Total_key") + " : "
        lblOrderDelivryDateTitle.text = getCommonString(key: "Order_delivery_date_key") + " : "
        
        btnOrderStatusOutlet.setTitle(getCommonString(key: "Order_status_key").uppercased(), for: .normal)
        btnReorderOutlet.setTitle(getCommonString(key: "Reorder_key").uppercased(), for: .normal)
        
        [lblOrderNumber,lblOrderDate,lblStoreName,lblOrderPaymentStatus,lblOrderFulfillStatus,lblOrderTotal,lblDeliveryDate].forEach { (lbl) in
            lbl?.showAnimatedSkeleton()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func stopAnimationSkeleton()
    {
        [lblOrderNumber,lblOrderDate,lblStoreName,lblOrderPaymentStatus,lblOrderFulfillStatus,lblOrderTotal,lblDeliveryDate].forEach { (lbl) in
            lbl?.hideSkeleton()
        }
        
    }
    
    func setupData(_ dict:JSON){
        lblOrderNumber.text = "#\(dict["order_num"].stringValue)"
        lblOrderDate.text = stringTodate(OrignalFormatter: dateFormatter.dateFormatter1.rawValue, YouWantFormatter: dateFormatter.dateFormatter2.rawValue, strDate: dict["order_dttime"].stringValue)
        lblOrderPaymentStatus.text =  dict["paid_status"].stringValue
        lblOrderFulfillStatus.text =  dict["fulfilment_status"].stringValue
        lblOrderTotal.text =  "\(strCurrenrcy) \(dict["order_price"].stringValue)"
        lblStoreName.text = dict["store_name"].stringValue
    }

}
