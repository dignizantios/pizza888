//
//  DrinkCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 09/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class DrinkCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var btnMinusOutlet: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnAddProductOutlet: UIButton!
    @IBOutlet weak var lblCustomizeType: UILabel!
    @IBOutlet weak var imgvwProduct: UIImageView!
    @IBOutlet weak var btnCustomizeSelectOutlet: UIButton!
    @IBOutlet weak var vwCustomize: CustomView!
    @IBOutlet weak var lblItemDescription: UILabel!
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblItemPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblItemDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [lblQuantity].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblCustomizeType].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        btnAddProductOutlet.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnAddProductOutlet.setTitleColor(UIColor.appThemeBlackColor, for: .normal)
        btnAddProductOutlet.setTitle(getCommonString(key: "Add_key"), for: .normal)

        imgvwProduct.layer.cornerRadius = imgvwProduct.frame.size.height/2
        imgvwProduct.layer.masksToBounds = true
        // Initialization code
        
        let dict = JSON()
//        self.perform(#selector(setTmpData(dict)), with: nil, afterDelay: 0.1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    func setData(dict:JSON)
    {
        /*if(dict["specification"].stringValue != "")
        {
            lblItemName.text = dict["product_name"].stringValue + "\n(\(dict["specification"].stringValue))"
        }else{*/
            lblItemName.text = dict["product_name"].stringValue
//        }
        
        lblItemDescription.text = dict["specification"].stringValue
        
        lblItemPrice.text = "\(strCurrenrcy) \(dict["product_price"].stringValue)"
        lblQuantity.text = dict["quantity"].stringValue
        
        vwCustomize.borderColor = UIColor.appThemeRedColor
        lblCustomizeType.textColor  = UIColor.appThemeRedColor
//        btnCustomizedOutlet.isSelected = false
        lblCustomizeType.text = "\(getCommonString(key: "Customize_key"))"
    }

}
