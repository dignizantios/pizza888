//
//  DatePickerVC.swift
//  USteerTeacher
//
//  Created by om on 1/4/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit


protocol datePickerDelegate {
    func setDateValue(dateValue : Date,type:Int)
}

struct datepickerMode
{
    var pickerMode : UIDatePicker.Mode
    
    init(customPickerMode:UIDatePicker.Mode)
    {
        self.pickerMode = customPickerMode
    }
}

class DatePickerVC: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet var vwButtonHeader : UIView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var datePicker : UIDatePicker!

    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
    var pickerDelegate : datePickerDelegate?
    var isSetMaximumDate = false
    var isSetMinimumDate = false

    var maximumDate = Date()
    var minimumDate = Date()
    var isSetDate = false
    var setDateValue : Date?
    
    //MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetupUI()
    }
    

}
//MARK: - Setup UI
extension DatePickerVC
{
    func SetupUI()
    {
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.white, for: .normal)
            $0?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        })
        
        btnDone.setTitle(getCommonString(key: "Done_key").uppercased(), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key").uppercased(), for: .normal)
        
        vwButtonHeader.backgroundColor = UIColor.appThemeRedColor
        
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
        if(isSetDate)
        {
            datePicker.setDate(setDateValue ?? Date(), animated: false)
        }
    }
}
//MARK: - Button Action
extension DatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        if datePickerCustomMode.pickerMode == .time
        {
            pickerDelegate?.setDateValue(dateValue: datePicker.date, type: 1)
        }
        else
        {
            pickerDelegate?.setDateValue(dateValue: datePicker.date, type: 0)
        }
        
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
