//
//  SelectFutureDateVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol delegateSelectFutureDate {
    func selectedDateTime(selectedDate:String,selectedTime:String)
}

class SelectFutureDateVC: UIViewController {
    
    //MARK:- Variablde Declaration
    
    var selectDateTime:delegateSelectFutureDate?
    var arrTimes:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var txtDeliveryTime: UITextField!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var txtDeliveryDate: UITextField!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSelectDateOutlet: CustomButton!
    
    //MARK:- Viewlife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

}

//MARK:- Setup UI

extension SelectFutureDateVC
{
    func setupUI()
    {
        [lblTitle].forEach { (lbl) in
            lbl?.backgroundColor = UIColor.appThemeRedColor
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 17, fontname: .medium)
        }
        
        lblTitle.text = getCommonString(key: "Select_future_date_key").uppercased()
        
        [lblDeliveryDate,lblDeliveryTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        lblDeliveryDate.text = getCommonString(key: "Pickup/Delivery_date_key").uppercased()
        lblDeliveryTime.text = getCommonString(key: "Pickup/Delivery_time_key").uppercased()
        
        [txtDeliveryDate,txtDeliveryTime].forEach { (txtField) in
            txtField?.textColor = UIColor.appThemeBlackColor
            txtField?.font = themeFont(size: 15, fontname: .regular)
            txtField?.delegate = self
        }
        
        txtDeliveryTime.placeholder = getCommonString(key: "Select_time_key").capitalized
        txtDeliveryDate.placeholder = getCommonString(key: "Select_date_key").capitalized
        
        btnSelectDateOutlet.setUpThemeButtonUI()
        btnSelectDateOutlet.backgroundColor = UIColor.appThemeRedColor
        btnSelectDateOutlet.setTitle(getCommonString(key: "Next_key"), for: .normal)
        
    }
}
//MARK:- Action Zone

extension SelectFutureDateVC
{
    @IBAction func btnSelectAction(_ sender:UIButton)
    {
        objOrder.strFutureOrderDate = txtDeliveryDate.text ?? ""
        objOrder.strFutureOrderTime = txtDeliveryTime.text ?? ""
        
        if objOrder.isCheckValidForFutureOrder()
        {
            self.dismiss(animated: true, completion: nil)
            self.selectDateTime?.selectedDateTime(selectedDate: txtDeliveryDate.text ?? "", selectedTime: txtDeliveryTime.text ?? "")
        }
        else
        {
            makeToast(message: objOrder.strValidationMessage)
        }
        
    }
    
    @IBAction func btnCloseAction(_ sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Textfiled Delegate

extension SelectFutureDateVC:UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDeliveryDate
        {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = true
            obj.isSetDate = true
            
            let currentDate = setDate(timeInterval:1)
            obj.setDateValue = currentDate
            obj.minimumDate = currentDate            
            
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        else if textField == txtDeliveryTime
        {
            self.view.endEditing(true)
            if arrTimes.count == 0 {
                return false
            }
            let obj = CommonPickerVC()
            obj.pickerDelegate = self
            obj.arrayPicker = arrTimes
            obj.selectedDropdown = .storeTime
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
}
//MARK:- Picker Delegate

extension SelectFutureDateVC : CommonPickerDelegate
{
    func setValuePicker(selectedDict: JSON) {
        self.txtDeliveryTime.text = selectedDict["time"].stringValue
    }
    
}



//MARK:- Picker Delegate

extension SelectFutureDateVC : datePickerDelegate
{
    func setDateValue(dateValue: Date,type:Int) {
        
        if type == 0
        {
            let strDate = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
            self.txtDeliveryDate.text = strDate
        }
        else
        {
            let strTime = DateToString(Formatter: "hh:mm a", date: dateValue)
            self.txtDeliveryTime.text = strTime
        }
    }
}

//MARK:- Private Method
extension SelectFutureDateVC
{
    func setDate(timeInterval:Int) -> Date {
        var beforeTimeInterval = DateComponents()
        beforeTimeInterval.day = timeInterval
        let beforeDate = Calendar.current.date(byAdding: beforeTimeInterval, to: Date())
        print("beforeDate \(String(describing: beforeDate))")
        return beforeDate!
    }
}
