//
//  ViewNav.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit

class ViewNav: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var btnLeftOutlet: UIButton!
    @IBOutlet weak var btnRightOutlet: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCartCount: CustomLabel!
    
    /*var strCartCount : String
    {
        didset
        {
            self.lblCartCount.text = strCartCount
        }
//        get {
//            let checkedCartItems = Array(realm.objects(Cart.self))
//            return "\(checkedCartItems.count)"
//        }
//        set {
////            let checkedCartItems = Array(realm.objects(Cart.self))
//            self.lblCartCount.text = strCartCount
//        }
    }*/
    
    //MARK:- View life Cycle
    
    override func awakeFromNib() {
       
        lblTitle.textAlignment = .center
        lblTitle.textColor = UIColor.white
        lblTitle.font = themeFont(size: 18, fontname: .semibold)
        
        lblCartCount.textAlignment = .center
        lblCartCount.textColor = UIColor.appThemeRedColor
        lblCartCount.backgroundColor = .white
        lblCartCount.font = themeFont(size: 12, fontname: .regular)
        
    }

}
