//
//  AddCardVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 24/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class AddCardVC: UIViewController {
    
    //MARK:- Variablde Declaration
    
    var arrMonth:[JSON] = []
    var arrYear:[JSON] = []
    var activateTextfield:Int = 0
    var dictCreditCardDetail = JSON()
    var dictDiscountAmt = JSON()
    var strComment = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblCardName: UILabel!
    @IBOutlet weak var txtCardName: UITextField!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var lblExpiryMonth: UILabel!
    @IBOutlet weak var txtExpiryMonth: UITextField!
    @IBOutlet weak var lblExpiryYear: UILabel!
    @IBOutlet weak var txtExpiryYear: UITextField!
    @IBOutlet weak var lblCVV: UILabel!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var btnAddCardOutlet: CustomButton!
    
 
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupYearArray()
        setupUI()
        setupMonthArray()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Credit_card_key").capitalized, type: .back, barType: .white)
    }

}

//MARK:- Setup UI
extension AddCardVC
{
    
    func setupUI()  {
        
        [lblCardName,lblCardNumber,lblExpiryMonth,lblExpiryYear,lblCVV].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = UIColor.black
        }
        
        [txtCardName,txtCardNumber,txtExpiryMonth,txtExpiryYear,txtCVV].forEach { (txtField) in
            txtField?.font = themeFont(size: 16, fontname: .regular)
            txtField?.textColor = .black
            txtField?.delegate = self
        }
        
        [txtCardName,txtCardNumber,txtCVV].forEach { (txtField) in
            txtField?.placeholder = getCommonString(key: "Enter_here_key")
        }
        
        [txtExpiryMonth,txtExpiryYear].forEach { (txtField) in
            txtField?.placeholder = getCommonString(key: "Select_key")
        }
      
        lblCardName.text = getCommonString(key: "Name_on_card_key").uppercased()
        lblCardNumber.text = getCommonString(key: "Card_number_key").uppercased()
        lblExpiryYear.text = getCommonString(key: "Expiry_year_key").uppercased()
        lblExpiryMonth.text = getCommonString(key: "Expiry_month_key").uppercased()

        lblCVV.text = getCommonString(key: "CVV_key").uppercased()
        
        btnAddCardOutlet.setUpThemeButtonUI()
        btnAddCardOutlet.backgroundColor = UIColor.appThemeRedColor
        btnAddCardOutlet.setTitle(getCommonString(key: "Pay_now_key").uppercased(), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtCardNumber)
        addDoneButtonOnKeyboard(textfield: txtCVV)
        
        getCreditCardDetail()
    }
}

//MARK:- UITextfiled Delegate

extension AddCardVC :UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtExpiryMonth
        {
            activateTextfield = 0
            self.view.endEditing(true)
            let obj = CommonPickerVC()
            obj.selectedDropdown = .addcard
            obj.pickerDelegate = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.arrayPicker = arrMonth
            self.present(obj, animated: false, completion: nil)
            return false
        }
        else if  textField == txtExpiryYear
        {
            if self.txtExpiryMonth.text == ""
            {
                makeToast(message: getValidationString(key: "Please_select_expiry_month_key"))
                self.txtExpiryYear.text = ""
                return false
            }
            activateTextfield = 1
            self.view.endEditing(true)
            let obj = CommonPickerVC()
            obj.selectedDropdown = .addcard
            obj.pickerDelegate = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.arrayPicker = arrYear
            self.present(obj, animated: false, completion: nil)
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*if textField == txtCVV
        {
            let limitLength = secutiryNumber
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }
        else if textField == txtCardNumber
        {
            let limitLength = accountNumber
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }*/
        /* else if textField == txtCardNumber
         {
         previousTextFieldContent = textField.text;
         previousSelection = textField.selectedTextRange;
         }*/
        return true
    }
}

//MARK:- Setup Card

extension AddCardVC
{
    func setupYearArray()
    {
        arrYear = []
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        print("year \(year)")
        
        for i in stride(from: year, to: year+50, by: 1) {
            var dict = JSON()
            dict["value"] = JSON(String(i))
            arrYear.append(dict)
        }
        print("arrYear \(arrYear)")
    }
    
    func setupMonthArray()
    {
        arrMonth = []
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        print("year \(year)")
        
        for i in stride(from: 1, to: 13, by: 1) {
            var dict = JSON()
            if i >= 10
            {
                dict["value"] = JSON(String(i))
            }
            else
            {
                dict["value"] = JSON("0"+String(i))
            }
            arrMonth.append(dict)
        }
        print("arrMonth \(arrMonth)")
    }
}

extension AddCardVC :CommonPickerDelegate
{
    func setValuePicker(selectedDict: JSON) {
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        if activateTextfield == 0
        {
            if selectedDict["value"].intValue <= month
            {
                if self.txtExpiryYear.text ==  ""
                {
                    self.txtExpiryMonth.text = selectedDict["value"].stringValue
                }
                else
                {
                    if Int(self.txtExpiryYear.text!)! <= year
                    {
                        makeToast(message: getValidationString(key: "Please_select_valid_expiry_month_key"))
                        self.txtExpiryMonth.text = ""
                    }
                    else
                    {
                        self.txtExpiryMonth.text = selectedDict["value"].stringValue
                    }
                }
            }
            else
            {
                self.txtExpiryMonth.text = selectedDict["value"].stringValue
            }
        }
        else
        {
            if self.txtExpiryMonth.text == ""
            {
                makeToast(message: getValidationString(key: "Please_select_valid_expiry_month_key"))
                self.txtExpiryYear.text = ""
            }
            else
            {
                if selectedDict["value"].intValue <= year
                {
                    if Int(self.txtExpiryMonth.text!)! <= month
                    {
                        makeToast(message: getValidationString(key: "Please_select_valid_expiry_year_key"))
                        self.txtExpiryYear.text = ""
                    }
                    else
                    {
                        self.txtExpiryYear.text = selectedDict["value"].stringValue
                        self.txtCVV.becomeFirstResponder()
                    }
                }
                else
                {
                    self.txtExpiryYear.text = selectedDict["value"].stringValue
                    self.txtCVV.becomeFirstResponder()
                }
            }
            
        }
    }
    
}



//MARK:- Action Zone

extension AddCardVC
{
    @IBAction func btnAddCardAction(_ sender:UIButton)
    {
        if dictCreditCardDetail.count == 0{
            makeToast(message: getValidationString(key: "Sorry_we_are_unable_to_use_credit_card_key"))
            return
        }
        objUser.strCardHolderName = txtCardName.text ?? ""
        objUser.strCardNumber = txtCardNumber.text ?? ""
        objUser.strExpiryMonth = txtExpiryMonth.text ?? ""
        objUser.strExpiryYear = txtExpiryYear.text ?? ""
        objUser.strCVV = txtCVV.text ?? ""
        
        if objUser.isCheckCardDetail()
        {
            OrderService()
        }
        else{
            makeToast(message: objUser.strValidationMessage)
        }
    }
    func NavigateToSuccess()
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "OrderSucessVC") as! OrderSucessVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Service
extension AddCardVC
{
    func OrderService()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kPayWithCreditCard)"
            
            print("URL: \(url)")
            
            let CheckUser = Array(globalRealm.objects(CheckoutUser.self))
            let objCheckOutUser = CheckUser[0]
            
            let finalPrice = Float(dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: (CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) + Float(objCheckOutUser.strSuburbCost)!) : getPriceFormatedValue(strPrice: CartTotalPrice() + Float(objCheckOutUser.strSuburbCost)!))
            
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
          
            formatter.numberStyle = .decimal
            let priceNew = formatter.string(from: (finalPrice! as NSNumber)) ?? "n/a"
            print("test",formatter.string(from: finalPrice! as NSNumber) ?? "n/a")
            let testPrice = String(formatter.string(from: (finalPrice! as NSNumber))  ?? "n/a")
            
            let vpnAmount = Int((Double(String(priceNew)) ?? 0.0)*100)
            print("vpnAmount:", vpnAmount)
            
            let param =  ["vpc_Version":dictCreditCardDetail["vpc_Version"].stringValue,
                          "vpc_Command":dictCreditCardDetail["vpc_Command"].stringValue,
                          "vpc_Orderinfo":strComment,
                          "vpc_Amount":"\(vpnAmount)",
                          "vpc_CardNum":self.txtCardNumber.text ?? "",
                          "vpc_CardExp":"\(self.txtExpiryYear.text?.suffix(2) ?? "")\(self.txtExpiryMonth.text ?? "")",
                          "vpc_CardSecurityCode":"\(self.txtCVV.text ?? "")",
                          "vpc_AVS_Street01":self.txtCardName.text ?? "",
                          "vpc_MerchTxnRef":"test",
                          "vpc_AccessCode":dictCreditCardDetail["vpc_AccessCode"].stringValue,
                          "vpc_Merchant":dictCreditCardDetail["vpc_Merchant"].stringValue,
                          "vpc_ReturnURL":dictCreditCardDetail["vpc_ReturnURL"].stringValue]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        let dictFinal = self.GetCheckOutCartItem()
                        self.CheckOutService(json : dictFinal)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func getCreditCardDetail()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetCrediCardDetail)"
            
            print("URL: \(url)")
            
            let param:[String:String] = [:]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.dictCreditCardDetail = json["data"].arrayValue[0]
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func CheckOutService(json : JSON)
    {
        print("Final order json - ",json)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCheckout_cash)"
            
            print("URL: \(url)")
            
            let orderUser =  Array(globalRealm.objects(CheckoutOrder.self))
            let objCheckOutOrder = orderUser[0]
            
            let CheckUser = Array(globalRealm.objects(CheckoutUser.self))
            let objCheckOutUser = CheckUser[0]
            
            var param:[String:String] =  ["ordertype":"\(objCheckOutOrder.strSelectedOrderType)",
                "address_name":objCheckOutUser.strName,
                "address_phone":objCheckOutUser.strPhoneNumber,
                "address_email":objCheckOutUser.strEmailAddress,
                "tickemail":objCheckOutUser.strReceiveMail,
                "ticksms":objCheckOutUser.strReceiveSMS,
                "amount_total":dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) : getPriceFormatedValue(strPrice: CartTotalPrice()),
                "future_date":objCheckOutOrder.strFutureOrderDate,
                "future_time":objCheckOutOrder.strFutureOrderTime,
                "prod_info":json.rawString() ?? "",
                "customer_id":getUserDetail("id") != "" ? getUserDetail("id") : "0",
                "disamnt":dictDiscountAmt.count != 0 ? dictDiscountAmt["disamt"].stringValue : "0",
                "store_id":objCheckOutOrder.strSelectedStoreId,
                "address_suburb":"",
                "timestatus":"1",
                "pay_type":selectPaymentType.creditCard.rawValue,
                "note" : strComment,
                "order_prefix":strOrderPrefix
            ]
            
            if objCheckOutOrder.strSelectedOrderType == 1 {
                param["unit_number"] = objCheckOutUser.strUnitNumber
                param["street_number"] = objCheckOutUser.strStreetNumber
                param["street_name"] = objCheckOutUser.strStreetName
                param["address_suburb"] = objCheckOutUser.strSuburbId
                param["delivery_instruction"] = objCheckOutUser.strDeliveryInsturction
                param["suburb_cost"] = objCheckOutUser.strSuburbCost
                param["amount_total"] = dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: (CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) + Float(objCheckOutUser.strSuburbCost)!) : getPriceFormatedValue(strPrice: CartTotalPrice() + Float(objCheckOutUser.strSuburbCost)!)
            }
            
            if objCheckOutOrder.strFutureOrderDate != "" && objCheckOutOrder.strFutureOrderDate != ""
            {
                param["timestatus"] = "2"
            }
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.deleteAllItemsfromCart()
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderSucessVC") as! OrderSucessVC
                        obj.strMsg = json["Fullmsg"].stringValue
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
        
    }
}

extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
