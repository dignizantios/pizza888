//
//  HalfParentVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 08/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import CarbonKit

class HalfParentVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictData = JSON()
    var dictTmpData = JSON()
    var arrHalfDetail:[JSON] = []
    var arrHalfMenu = [String]()
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var intCheckHalfCount = Int()
    var isSelectedFirstHalf = false
    var isSelectedSecondHalf = false

    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgHalfDetail: CustomImageView!
    @IBOutlet weak var lblItemName:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var viewHalf:UIView!
    @IBOutlet weak var heightOfAddtoCart: NSLayoutConstraint!
    @IBOutlet weak var btnAddToCartOutlet: UIButton!
    @IBOutlet weak var btnResetDealOutlet: UIButton!
    @IBOutlet weak var txtViewDesciption: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
     //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        dictTmpData = dictData
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dictData["halfName"].stringValue, type: .backWithCart, barType: .white)
    }
    
    //MARK:- Setup
    
    func setupUI()  {
        
        isSelectedFirstHalf = false
        isSelectedSecondHalf = false
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        lblItemName.text = dictData["halfName"].stringValue
        lblPrice.text = "\(strCurrenrcy) \(dictData["halfPrice"].stringValue)"
        lblDescription.text = dictData["halfdiscription"].stringValue
        
        arrHalfMenu = []
//        arrHalfMenu.append(getCommonString(key: "1st_half_key"))
//        arrHalfMenu.append(getCommonString(key: "2nd_half_key"))
        
        arrHalfMenu.append(dictData["sub_data"].arrayValue[0]["name"].stringValue)
        arrHalfMenu.append(dictData["sub_data"].arrayValue[1]["name"].stringValue)
        
        btnResetDealOutlet.isHidden = true
        
        [btnAddToCartOutlet,btnResetDealOutlet].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .semibold)
            btn?.backgroundColor = UIColor.appThemeRedColor
        }
        
        btnAddToCartOutlet.setTitle(getCommonString(key: "Add_to_cart_key").capitalized, for: .normal)
        btnResetDealOutlet.setTitle(getCommonString(key: "Reset_key").capitalized, for: .normal)
        
        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.arrHalfMenu , delegate: self)
        self.carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.viewHalf)
        
        self.style()

        self.imgHalfDetail.layer.cornerRadius = self.imgHalfDetail.frame.size.height/2
        self.imgHalfDetail.layer.masksToBounds = true
        self.imgHalfDetail.sd_setShowActivityIndicatorView(true)
        self.imgHalfDetail.sd_setIndicatorStyle(.gray)
        self.imgHalfDetail.sd_setImage(with: dictData["image"].url, placeholderImage: nil, options: .lowPriority, completed: nil)
        hideAddtoCartButton()
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_description_here_key")
        }
        
        [txtViewDesciption].forEach { (txtView) in
            txtView?.font = themeFont(size: 16, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 5, left: 8, bottom: 8, right: 8)
        }
    }

}

//MARK:- Textview Delegate

extension HalfParentVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Action Zone

extension HalfParentVC
{
    @IBAction func btnAddtoCartAction(_ sender:UIButton)
    {
        print("Final half half data - ",dictData)
        dictData["half_extra_instruction"].stringValue = self.txtViewDesciption.text ?? ""
        AddToCart(dict: dictData)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetDealAction(_ sender:UIButton)
    {
        /*self.arrDealDetail = self.arrTmpDealDetail
        self.AddExtraInformation()
        self.tblDealDetail.reloadData()
        lblPrice.text = "\(strCurrenrcy) \(mainDealPrice)"
        dictData["dealPrice"].floatValue = mainDealPrice
        finalDealPrice = dictData["dealPrice"].floatValue
        hideAddtoCartButton()*/
        dictData = dictTmpData
        setupUI()
        self.btnResetDealOutlet.isHidden = true
    }
}

//MARK:- Private Zone

extension HalfParentVC
{
    func hideAddtoCartButton()
    {
//        self.heightOfAddtoCart.constant = 0
        self.btnAddToCartOutlet.isHidden = true
    }
    
    func showAddtoCartButton()
    {
//        self.heightOfAddtoCart.constant = 50
        self.btnAddToCartOutlet.isHidden = false
    }
    func SetUpPrice()
    {
        print("dictData \(dictData)")
        let dictHalf1 = dictData["Half1"]
        let dictHalf2 = dictData["Half2"]
        
        var half1AddedPrice : Float = 0.0
        var half2AddedPrice : Float = 0.0
        
        if(dictHalf1["customized"].stringValue == "1")
        {
//            half1AddedPrice = half1AddedPrice + dictHalf1["addedTopingsPrice"].floatValue
            
            half1AddedPrice = half1AddedPrice + dictHalf1["finalPrice"].floatValue
        }
        if(dictHalf2["customized"].stringValue == "1")
        {
            
//            half2AddedPrice = half2AddedPrice + dictHalf2["addedTopingsPrice"].floatValue
            half2AddedPrice = half2AddedPrice + dictHalf2["finalPrice"].floatValue
        }
        
        let totalPrice = dictData["product_price"].floatValue + half1AddedPrice + half2AddedPrice
        
        lblPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: totalPrice))"
        dictData["halfPrice"].floatValue = totalPrice
        
    }
}

//MARK: - Carbon kit
extension HalfParentVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let width = UIScreen.main.bounds.size.width
        
        carbonTabSwipeNavigation.toolbarHeight.constant = 60
        
        let tabWidth = (width / CGFloat(arrHalfMenu.count))
        let indicatorcolor: UIColor = UIColor.appThemeRedColor
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        
        carbonTabSwipeNavigation.setNormalColor(UIColor.appThemeLightGrayColor, font: themeFont(size: 17, fontname: .regular))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.appThemeRedColor, font: themeFont(size: 17, fontname: .semibold))
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        
        let vc = objStoryboard.instantiateViewController(withIdentifier: "HalfChildVC") as! HalfChildVC
        vc.dictData = dictData
        vc.index = Int(index)
        vc.handleHalfCustomize = {[weak self](count,dict) in
            self?.btnResetDealOutlet.isHidden = false
            if(count == 0)
            {
                self?.isSelectedFirstHalf = true
                self?.dictData["Half1"] = dict
            }
            else if(count == 1)
            {
                self?.isSelectedSecondHalf = true
                self?.dictData["Half2"] = dict
            }
            
            if(self?.isSelectedFirstHalf == true && self?.isSelectedSecondHalf == true)
            {
                self!.showAddtoCartButton()
            }
            self?.SetUpPrice()
            /*self?.intCheckHalfCount += count
            if (self?.intCheckHalfCount)! >= 2
            {
                self!.showAddtoCartButton()
            }*/
        }
        return vc
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        
    }
    
}
