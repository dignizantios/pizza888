//
//  SelectItemsVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON



class SelectItemsVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrProduct:[JSON] = []
    var arrMenu = [String]()   
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    //var items = NSArray()
    
    
    //MARK:- Outlet Zone
  
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewOfBottom: UIView!
    @IBOutlet weak var viewOfBottomNavigation: UIView!

    let vwnav = Bundle.main.loadNibNamed("ViewNav", owner: nil, options: nil)?[0] as? ViewNav
    var cartCount : Int = 0
    {
        didSet
        {
            vwnav?.lblCartCount.text = "\(GetCartItemsCount())"
        }
    }
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SetTotalPriceValue), name: NSNotification.Name(rawValue: "SetTotalPriceValue"), object: nil)
        
        setupUI()
        getCategoryList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitleAndSideMenuAndCartItemsList(strTitle: getCommonString(key: "Select_items_key"), type: .backWithCart, barType: .white)
        if GetCartItemsCount() == 0 {
            vwnav?.lblCartCount.isHidden = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        
        viewOfBottom.roundCorners([.topRight,.bottomRight], radius: viewOfBottom.frame.size.height/2)
        
        viewOfBottomNavigation.layer.cornerRadius = viewOfBottomNavigation.frame.size.height/2
        viewOfBottomNavigation.layer.masksToBounds = true
    }
    
    func setUpNavigationBarWithTitleAndSideMenuAndCartItemsList(strTitle : String,type:selectNavigation,barType:navigationType)
    {
        let bounds = self.navigationController!.navigationBar.bounds
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + navHeight)
        
        // self.navigationController?.isNavigationBarHidden = false
        
        vwnav?.frame = CGRect(x: 0, y: 0, width: self.navigationController?.navigationBar.frame.width ?? 320, height: vwnav?.frame.height ?? 44)
            
            if barType == .white
            {
                vwnav?.imgHeader.image = UIImage(named: "bh_header_shadow")
            }
            vwnav?.lblTitle.text = strTitle
            vwnav?.btnRightOutlet.isHidden = true
            vwnav?.lblCartCount.isHidden = true
            vwnav?.btnLeftOutlet.tag = type.rawValue
        
        
            let checkedCartCount = cartCount
            
            if type == .sideMenuWithCart
            {
                if checkedCartCount == 0
                {
                    vwnav?.lblCartCount.isHidden = true
                }
                else
                {
                    vwnav?.lblCartCount.isHidden = false
                    vwnav?.lblCartCount.text = String(checkedCartCount)
                }
                vwnav?.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
                
                vwnav?.btnRightOutlet.isHidden = false
                vwnav?.btnRightOutlet.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
            }
            else if type == .back
            {
                vwnav?.btnLeftOutlet.setImage(UIImage(named: "ic_back_arrow_header"), for: .normal)
                vwnav?.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
            else if type == .sidemenu
            {
                vwnav?.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
            else if type == .backWithCart
            {
                if checkedCartCount == 0
                {
                    vwnav?.lblCartCount.isHidden = true
                }
                else
                {
                    vwnav?.lblCartCount.isHidden = false
                    vwnav?.lblCartCount.text = String(checkedCartCount)
                }
                
                vwnav?.btnRightOutlet.isHidden = false
                vwnav?.btnRightOutlet.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
                vwnav?.btnLeftOutlet.setImage(UIImage(named: "ic_back_arrow_header"), for: .normal)
                vwnav?.btnLeftOutlet.isHidden = false
                vwnav?.btnLeftOutlet.addTarget(self, action: #selector(btnBackSideAction(_:)), for: .touchUpInside)
            }
        
        self.navigationController?.view.addSubview(vwnav!)
        
        
    }
}

//MARK:- Setup

extension SelectItemsVC{
    
    func setupUI()
    {
        [lblPrice].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }       
        
        //style()
        setCartPrice()

    }
    @objc func SetTotalPriceValue()
    {
        setCartPrice()

    }
    func setCartPrice()
    {
        cartCount = GetCartItemsCount()
        lblPrice.text = "\(getCommonString(key: "Total_key")) : \(strCurrenrcy) \(getPriceFormatedValue(strPrice: CartTotalPrice()))"
    }
}

//MARK:- Action

extension SelectItemsVC
{
    @IBAction func btnCheckOutAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - Carbon kit
extension SelectItemsVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        
        let width = UIScreen.main.bounds.size.width
        
        carbonTabSwipeNavigation.toolbarHeight.constant = 60
        
        //let tabWidth = (width / CGFloat(items.count))
        let indicatorcolor: UIColor = UIColor.appThemeRedColor
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        
       /* carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false*/
        
       // carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.appThemeLightGrayColor, font: themeFont(size: 17, fontname: .regular))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.appThemeRedColor, font: themeFont(size: 17, fontname: .semibold))
       
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        var selectedCatItem = selectCategoryItem.regular
        switch  arrProduct[Int(index)]["type"].intValue
        {
        case 1:
            selectedCatItem = .drink
        case 2:
            selectedCatItem = .regular
        case 3:
            selectedCatItem = .deal
        case 4:
            selectedCatItem = .half
//        case 5:
//            selectedCatItem = .pasta
        default:
            selectedCatItem = .regular
        }
        let vc = objStoryboard.instantiateViewController(withIdentifier: "SelectItemChildVC") as! SelectItemChildVC
        vc.selectedCategoryItem = selectedCatItem
        vc.dictProduct =  arrProduct[Int(index)]
        return vc
        
    }
    
}

//MARK:- Service

extension SelectItemsVC
{
    func getCategoryList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetCategoriesProduct)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id":dictStoreDetail["id"].stringValue,
                          "flag":"\(intDeliveryType)"]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.viewOfBottom.isHidden = false
                        
                        self.arrProduct = []
                        self.arrProduct = json["data"].arrayValue
                        self.arrMenu = []
                        for i in 0..<self.arrProduct.count
                        {
                            let dict = self.arrProduct[i]
                            self.arrMenu.append(dict["category_name"].stringValue)
                        }
                        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.arrMenu , delegate: self)
                        self.carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.vwMain)
                        
                        self.style()
                    }
                    else
                    {
                        self.viewOfBottom.isHidden = true
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

