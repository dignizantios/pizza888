//
//  DeliveryTypeVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class DeliveryTypeVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dict = JSON()
    var isComeFromA = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPickup: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblReorder: UILabel!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dict["store_name"].stringValue, type: .backWithCart, barType: .black)
    }
    
    //MARK:- Setup
    
    func setupUI()  {
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.white
        }
        lblTitle.text = getCommonString(key: "How_can_we_help_you_key").uppercased()
        
        [lblPickup,lblReorder,lblDelivery].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.black
        }
        
        lblPickup.text = getCommonString(key: "Pickup_key")
        lblDelivery.text = getCommonString(key: "Delivery_key")
        lblReorder.text = getCommonString(key: "Reorder_key")
    }
    
}

//MARK:- Action Zone
extension DeliveryTypeVC
{
    @IBAction func btnPickupAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryDetailVC") as! DeliveryDetailVC
        obj.selectType = .pickup
        obj.dict = dict
        objOrder.strSelectedOrderType = selectOrderType.pickup
        objUser = User()
        objUser.strUnitNumber = ""
        objUser.strStreetNumber = ""
        objUser.strStreetName = ""
        objUser.strSuburbName = ""
        objUser.strDeliveryInsturction = ""
        objUser.strSuburbId = ""
        objUser.strSuburbCost = ""
        SaveOrderDetailsToDefaults()
        intDeliveryType = selectOrderType(rawValue: selectOrderType.pickup.rawValue)!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnDelivryAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryDetailVC") as! DeliveryDetailVC
        obj.selectType = .delivery
        obj.dict = dict
        objOrder.strSelectedOrderType = selectOrderType.delivery
        SaveOrderDetailsToDefaults()

        intDeliveryType = selectOrderType(rawValue: selectOrderType.delivery.rawValue)!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnReoderAction(_ sender:UIButton)
    {
         
    }
}
