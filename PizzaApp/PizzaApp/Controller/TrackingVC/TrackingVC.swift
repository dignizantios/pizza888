//
//  TrackingVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 24/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class TrackingVC: UIViewController {
    
    //MARK:- Varible Declaration
    
    var typeDD = DropDown()
    var arrType : [String] = []
    var strStoreId = String()
    var arrHome:[JSON] = []
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblChooseOrderTitle: UILabel!
    @IBOutlet weak var txtChooseStore: UITextField!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var txtOrderNumber: UITextField!
    @IBOutlet weak var btnTrackOrderOutlet: CustomButton!
    
    //MARK:- Viewlife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Tracking_key"), type: .sidemenu, barType: .white)
    }
    
    override func viewDidLayoutSubviews() {
        configureDropdown(dropdown: typeDD, sender: txtChooseStore)
    }

}

//MARK:- Setup UI

extension TrackingVC
{
    func setupUI()
    {
//        arrType = [getCommonString(key: "Cranbourne_west_key"),getCommonString(key: "Cranbourne_north_key"),getCommonString(key: "Balwyn_store_key")]
        
        [lblChooseOrderTitle,lblOrderNumber].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .medium)
            lbl?.textColor = UIColor.black
        }
        
        lblChooseOrderTitle.text = getCommonString(key: "Choose_store_key")
        lblOrderNumber.text = getCommonString(key: "Order_number_key")
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = getCommonString(key: "Choose_our_store_from_below_key")
        
        [txtChooseStore,txtOrderNumber].forEach { (txtField) in
            txtField?.font = themeFont(size: 17, fontname: .regular)
            txtField?.textColor = .black
            txtField?.delegate = self
        }
        
        txtChooseStore.placeholder = getCommonString(key: "Select_store_key")
        txtOrderNumber.placeholder = getCommonString(key: "Enter_here_key")
        
        btnTrackOrderOutlet.setUpThemeButtonUI()
        btnTrackOrderOutlet.backgroundColor = UIColor.appThemeRedColor
        btnTrackOrderOutlet.setTitle(getCommonString(key: "Track_order_key").capitalized, for: .normal)
        
        typeDD.dataSource = arrType
        typeDD.selectionAction = { (index: Int, item: String) in
            self.txtChooseStore.text = item
            self.strStoreId = self.arrHome[index]["id"].stringValue
        }
        getStoreList()
    }

}

//MARK:- UITextfiled Delegate

extension TrackingVC :UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtChooseStore
        {
            typeDD.show()
            return false
        }
        return true
    }
}

//MARK:- Action Zone

extension TrackingVC
{
    @IBAction func btnTrackOrder(_ sender:UIButton)
    {
        objOrder.strSelectStore = self.txtChooseStore.text ?? ""
        objOrder.strOrderNumber = self.txtOrderNumber.text ?? ""
        
        if objOrder.isCheckValidForTracking() {
            getTrackingOrder()
        } else {
            makeToast(message: objOrder.strValidationMessage)
        }
    }
}

//MARK:- Service

extension TrackingVC {
    
    func getTrackingOrder()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kTrackOrder)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "ordid":self.txtOrderNumber.text ?? "",
                          "store_id":strStoreId
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderStatusVC") as! OrderStatusVC
                        obj.dictOrderTrack = json["data"]
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }                   
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func getStoreList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kStoreList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrHome = []
                        self.arrHome = json["data"].arrayValue
                        self.arrType = self.arrHome.map({$0["store_name"].stringValue })
                        self.typeDD.dataSource = self.arrType
                    }
                    else
                    {
                        self.arrHome = []
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}
