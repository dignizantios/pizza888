//
//  RegisterVC.swift
//  Demo1
//
//  Created by om on 12/26/18.
//  Copyright © 2018 om. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

protocol delegateEditProfile {
    func editProfileDelegate()
}


func setUpThemeButtonUI(button:UIButton)
{
    button.layer.cornerRadius = button.frame.size.height/2
    button.layer.masksToBounds = true
    button.backgroundColor = UIColor.appThemeRedColor
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.font = themeFont(size: 20, fontname: .semibold)
}


class RegisterVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var selectedController = selectProfile.register
    var delegateProfile : delegateEditProfile?
    var dictData = JSON()
    
    //MARK:- Outlet
    
    
    @IBOutlet weak var vwTerms: UIStackView!
    @IBOutlet weak var vwConfirmPassword: UIView!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var heightOfvwRegister: NSLayoutConstraint!
    @IBOutlet weak var vwRegister: UIView!
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblPhone : UILabel!
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblPassword : UILabel!
    @IBOutlet var lblConfirmPassword : UILabel!
    
    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtPhone : UITextField!
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtConfirmPassword : UITextField!
    
    @IBOutlet var vwEmail : UIView!

    
    @IBOutlet var btnRegister : UIButton!
    @IBOutlet var btnIAgreeAccept : UIButton!

    @IBOutlet var lblIAgreeAcceptTitle : UILabel!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var viewOfAddress: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtviewAddress: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    
    //MARK:- Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SetUpUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .register
        {
            vwEmail.backgroundColor = UIColor.white
            let bounds = self.navigationController!.navigationBar.bounds
            self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 0)
            
            if let viewWithTag = self.navigationController?.view.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
            }
            self.viewOfAddress.isHidden = true
            
            self.navigationController?.isNavigationBarHidden = true
        }
        else
        {
            setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Edit_Profile_key").capitalized, type: .back, barType: .white)
           // heightOfvwRegister.constant = 0
            [vwRegister,vwPassword,vwConfirmPassword,vwTerms].forEach { (vw) in
                vw?.isHidden = true
            }
            viewOfAddress.isHidden = false
            vwEmail.backgroundColor = UIColor.appThemeLightGrayColor.withAlphaComponent(0.25)
            self.lblAddress.text = getCommonString(key: "Address_key")
            
            lblPlaceholder.textColor = UIColor.appThemeLightGrayColor
            lblPlaceholder.font = themeFont(size: 16, fontname: .regular)
            lblPlaceholder.text = getCommonString(key: "Enter_here_key")
            
            [txtviewAddress].forEach { (txtView) in
                txtView?.font = themeFont(size: 16, fontname: .regular)
                txtView?.textColor = .black
                txtView?.delegate = self
                txtView?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
            }
         
            self.lblTitle.isHidden = true
            self.btnBackOutlet.isHidden = true
            self.txtEmail.isUserInteractionEnabled = false
            btnRegister.setTitle(getCommonString(key: "Edit_Profile_key").uppercased(), for: .normal)
            setData(dict: dictData)
            
        }
       
      //  setUpNavigationBarWithTitleAndBack(strTitle:getCommonString(key: "Register_key"))
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK:- Setdata

extension RegisterVC{
    func setData(dict:JSON)
    {
        txtName.text = dict["username"].stringValue
        txtEmail.text = dict["email_id"].stringValue
        txtviewAddress.text = dict["address"].stringValue
        txtPhone.text = dict["phone"].stringValue
        
        if dict["address"].stringValue == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
}
//MARK:- UI setup
extension RegisterVC
{
    func SetUpUI()
    {
        
        lblTitle.textColor = UIColor.appThemeRedColor
        lblTitle.text = getCommonString(key: "Register_key")
        lblTitle.font = themeFont(size: 24, fontname: .semibold)
        
        viewOfAddress.isHidden = true
        
        [lblName,lblPhone,lblEmail,lblPassword,lblConfirmPassword,lblAddress].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 16, fontname: .regular)
        }
        
        [txtName,txtPhone,txtEmail,txtPassword,txtConfirmPassword].forEach { (textfield) in
            textfield?.font = themeFont(size: 16, fontname: .regular)
            textfield?.textColor = UIColor.black
            textfield?.placeholder = getCommonString(key: "Enter_here_key")
        }
        
        [txtPassword,txtConfirmPassword].forEach { (textfield) in
            textfield?.placeholder = "********"
        }
        
        
        lblName.text = getCommonString(key: "Name_key").uppercased()
        lblPhone.text = getCommonString(key: "Phone_key").uppercased()
        lblEmail.text = getCommonString(key: "Email_key").uppercased()
        lblPassword.text = getCommonString(key: "Password_key").uppercased()
        lblConfirmPassword.text = getCommonString(key: "Confirm_password_key").uppercased()

        addDoneButtonOnKeyboard(textfield: txtPhone)
//
        btnRegister.setTitle(getCommonString(key: "Register_key").uppercased(), for: .normal)        
        
        setUpThemeButtonUI(button: btnRegister)
        
        setUIForAgreeTermsPrivacy()
        
    }
}

//MARK:- Button Action
extension RegisterVC
{
    @IBAction func btnBackAction(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAgreeTermsAction(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func btnRegisterAction(_ sender : UIButton)
    {     
        objUser = User()
        objUser.strName = self.txtName.text ?? ""
        objUser.strPhoneNumber = self.txtPhone.text ?? ""
        objUser.strEmailAddress = self.txtEmail.text ?? ""
        objUser.strOldPassword = self.txtPassword.text ?? ""
        objUser.strConfirmPassword = self.txtConfirmPassword.text ?? ""
        objUser.isTermsCondition =  btnIAgreeAccept.isSelected
        objUser.strAddress = self.txtviewAddress.text ?? ""
        
        if selectedController == .editProfile
        {
            if objUser.isEditProfile()
            {
                editUserRegister()
            }
            else
            {
                makeToast(message: objUser.strValidationMessage)
            }
        }
        else{
            if objUser.isRegistration()
            {
                userRegister()
            }
            else
            {
                makeToast(message: objUser.strValidationMessage)
            }
        }
        
        
        
    }
}

//MARK:- Textview Delegate

extension  RegisterVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
//MARK:- Textfield methods
extension RegisterVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
//MARK:- Other Method
extension RegisterVC
{
    func setUIForAgreeTermsPrivacy()
    {
        //
        let str1 = getCommonString(key: "I_agree_to_the_key")
        let str2 = getCommonString(key: "Terms_condition_key")
        let str3 = getCommonString(key: "Privacy_policy_key").capitalized
        
        let str = str1 + " " + str2 + " \(getCommonString(key: "And_key")) " + str3
        let interactableText = NSMutableAttributedString(string:str)
        
        
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        
        
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 14, fontname: .regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeRedColor, range: rangeTerms)
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeRedColor, range: rangePolicy)
        
        lblIAgreeAcceptTitle.attributedText = interactableText
        
       
        
        let tapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblIAgreeAcceptTitle.addGestureRecognizer(tapGeture)


    }
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
    {
        let str1 = getCommonString(key: "I_agree_to_the_key")
        let str2 = getCommonString(key: "Terms_condition_key")
        let str3 = getCommonString(key: "Privacy_policy_key").capitalized
        let str = str1 + " " + str2 + " \(getCommonString(key: "And_key")) " + str3
        
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        
        
        let checkClickedTerms = gesture.didTapAttributedTextInLabel(lblIAgreeAcceptTitle,targetRange : rangeTerms)
        
        if(checkClickedTerms)
        {
            print("Tapped terms")
            var dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Terms_condition_key").capitalized
            dict["image"].stringValue = "ic_tnc_setting_screen"
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
            obj.dict = dict
            obj.selectedPolicyType = .TermsCondition
            obj.selectedRedirectVC = .register
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        
        let checkClickedPrivacy = gesture.didTapAttributedTextInLabel(lblIAgreeAcceptTitle,targetRange : rangePolicy)
        
        if(checkClickedPrivacy)
        {
            var dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Privacy_policy_key").capitalized
            dict["image"].stringValue = "ic_privacy_policy_setting_screen"
            
            print("Tapped Privacy policy")
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
            obj.dict = dict
            obj.selectedPolicyType = .PrivacyPolicy
            obj.selectedRedirectVC = .register
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        
    }
}
extension UITapGestureRecognizer
{
    func didTapAttributedTextInLabel(_ label: UILabel, targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x:(labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y : locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

//MARK:- Service

extension RegisterVC
{
    func userRegister()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kRegi)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "username":objUser.strName,
                          "email_id":objUser.strEmailAddress,
                          "password":objUser.strOldPassword,
                          "phone":objUser.strPhoneNumber,
                          "device_token":"",
                          "register_id":"",
                          "device_type":strDeviceType]
        
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func editUserRegister()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kSaveUserProfile)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "username":objUser.strName,
                          "email_id":objUser.strEmailAddress,
                          "phone":objUser.strPhoneNumber,
                          "id":getUserDetail("id"),
                          "access_token" : getUserDetail("access_token"),
                          "address": objUser.strAddress,
                          "id_update": "1"]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.delegateProfile?.editProfileDelegate()
                        makeToast(message: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}


