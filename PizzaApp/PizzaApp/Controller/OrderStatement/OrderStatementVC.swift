//
//  OrderStatementVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 18/06/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class OrderStatementVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictOrder = JSON()
    var dictPaypalDetail = JSON()
    var selectedController = selectPaymentOrderType.order
    var dictDiscountAmt = JSON()
    var arrayCart : [JSON] = []
    var strProductName = [String]()
    var strComment = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var wkWebView: WKWebView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedController == .order {
            wkWebView.load(URLRequest(url: URL(string: "http://tech88.com.au/webview/\(dictOrder["ordid"].stringValue)/\(getUserDetail("id"))")!))
//            wkWebView.load(URLRequest(url: URL(string: "https://www.youtube.com/embed/QYh6mYIJG2Y")!))
            
        } else {
            arrayCart = GetCartData()
            for i in 0..<arrayCart.count {
                let dictCart = arrayCart[i]
                strProductName.append(setUpData(dict: dictCart))
            }
            getPaypalDetail()
            wkWebView.navigationDelegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .paypal {
             setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Payment_key").capitalized, type: .back, barType: .white)
        } else {
             setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Order_statement_key").capitalized, type: .back, barType: .white)
        }
       
    }

}

//MARK:- Delegate Method

extension OrderStatementVC:WKNavigationDelegate,WKUIDelegate{
   
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            print("navigationAction URL \(urlStr)")
            if urlStr.contains("checkout/done"){
                print("Got it")
                let dictFinal = GetCheckOutCartItem()
                CheckOutService(json : dictFinal)
            }
            if urlStr.contains("checkout/cart"){
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopLoader()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        showLoader()
    }
    
    
}

//MARK:- Get Item

extension OrderStatementVC {
    
    func setUpData(dict:JSON) -> String
    {
        print("dict - ",dict)
        var strProductName = ""
        
        if(dict["type"].stringValue == "2")
        {
            if(dict["customized"].stringValue == "1")
            {
                if(dict["isTypePasta"].stringValue == "1")
                {
                    strProductName = dict["product_name"].stringValue
                }else
                {
                    strProductName = dict["product_name"].stringValue + "(\(dict["pizzaSize"].stringValue))"
                }
                
            }else if(dict["customized"].stringValue == "0")
            {
                if(dict["pizzaSize"].stringValue != "")
                {
                    if(dict["isTypePasta"].stringValue == "1")
                    {
                        strProductName = dict["product_name"].stringValue
                        
                    }else
                    {
                        strProductName = dict["product_name"].stringValue + "(\(dict["pizzaSize"].stringValue))"
                    }
                    
                    
                }else{
                    strProductName = dict["product_name"].stringValue
                }
            }
//            lblItemName.text = strProductName
            return strProductName
        }
        else if(dict["type"].stringValue == "3") //Deal
        {
            /*
             objCart.deal_id = dict["dealid"].intValue
             objCart.deal_name = dict["dealName"].stringValue
             objCart.deal_description = dict["dealdiscription"].stringValue
             objCart.deal_price = dict["dealPrice"].stringValue
             
             */
//            lblItemName.text = dict["dealName"].stringValue
            return dict["dealName"].stringValue
//            lblItemPrice.text = "\(strCurrenrcy) \(getPriceFormatedValue(strPrice: dict["dealPrice"].floatValue))"
        }
        else if(dict["type"].stringValue == "4")
        {
//            lblItemName.text = dict["halfName"].stringValue
            return dict["halfName"].stringValue
        }
        else{
            if(dict["specification"].stringValue != "")
            {
                return dict["product_name"].stringValue + "\n(\(dict["specification"].stringValue))"
//                lblItemName.text = dict["product_name"].stringValue + "\n(\(dict["specification"].stringValue))"
            }else{
//                lblItemName.text = dict["product_name"].stringValue
                return dict["product_name"].stringValue
            }
        }
    }
}

//MARK:- Service

extension OrderStatementVC {
    
    func getPaypalDetail()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetCrediCardDetail)"
            
            print("URL: \(url)")
            
            let param:[String:String] = [:]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.dictPaypalDetail = json["data"].arrayValue[1]
                        self.payWithPaypal()
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func payWithPaypal()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kPayWithPaypal)"
            
            print("URL: \(url)")
            
            let CheckUser = Array(globalRealm.objects(CheckoutUser.self))
            let objCheckOutUser = CheckUser[0]
            
            let param:[String:String] =  ["business":"\(dictPaypalDetail["business"].stringValue)",
                "cmd":"\(dictPaypalDetail["cmd"].stringValue)",
                "item_name":strProductName.joined(separator: ","),
                "item_number":"\(strProductName.count)",
                "amount":dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: (CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) + Float(objCheckOutUser.strSuburbCost)!) : getPriceFormatedValue(strPrice: CartTotalPrice() + Float(objCheckOutUser.strSuburbCost)!),
                "currency_code":"\(dictPaypalDetail["currency_code"].stringValue)",
                "cancel_return":"\(dictPaypalDetail["cancel_return"].stringValue)",
                "return":"\(dictPaypalDetail["return"].stringValue)"
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.wkWebView.load(URLRequest(url: json["data"].url ?? URL(string: "https://www.google.com/")!))
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func CheckOutService(json : JSON)
    {
        //        makeToast(message: "checkout API is remaining")
        print("Final order json - ",json)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCheckout_cash)"
            
            print("URL: \(url)")
            
            let orderUser =  Array(globalRealm.objects(CheckoutOrder.self))
            let objCheckOutOrder = orderUser[0]
            
            let CheckUser = Array(globalRealm.objects(CheckoutUser.self))
            let objCheckOutUser = CheckUser[0]
            
            var param:[String:String] =  ["ordertype":"\(objCheckOutOrder.strSelectedOrderType)",
                "address_name":objCheckOutUser.strName,
                "address_phone":objCheckOutUser.strPhoneNumber,
                "address_email":objCheckOutUser.strEmailAddress,
                "tickemail":objCheckOutUser.strReceiveMail,
                "ticksms":objCheckOutUser.strReceiveSMS,
                "amount_total":dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) : getPriceFormatedValue(strPrice: CartTotalPrice()),
                "future_date":objCheckOutOrder.strFutureOrderDate,
                "future_time":objCheckOutOrder.strFutureOrderTime,
                "prod_info":json.rawString() ?? "",
                "customer_id":getUserDetail("id") != "" ? getUserDetail("id") : "0",
                "disamnt":dictDiscountAmt.count != 0 ? dictDiscountAmt["disamt"].stringValue : "0",
                "store_id":objCheckOutOrder.strSelectedStoreId,
                "address_suburb":"",
                "timestatus":"1",
                "pay_type":selectPaymentType.paypal.rawValue,
                "note" : strComment,
                "order_prefix":strOrderPrefix
            ]
            
            if objCheckOutOrder.strSelectedOrderType == 1 {
                param["unit_number"] = objCheckOutUser.strUnitNumber
                param["street_number"] = objCheckOutUser.strStreetNumber
                param["street_name"] = objCheckOutUser.strStreetName
                param["address_suburb"] = objCheckOutUser.strSuburbId
                param["delivery_instruction"] = objCheckOutUser.strDeliveryInsturction
                param["suburb_cost"] = objCheckOutUser.strSuburbCost
                param["amount_total"] = dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: (CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) + Float(objCheckOutUser.strSuburbCost)!) : getPriceFormatedValue(strPrice: CartTotalPrice() + Float(objCheckOutUser.strSuburbCost)!)
            }
            
            if objCheckOutOrder.strFutureOrderDate != "" && objCheckOutOrder.strFutureOrderDate != ""
            {
                param["timestatus"] = "2"
            }
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.deleteAllItemsfromCart()
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderSucessVC") as! OrderSucessVC
                        obj.strMsg = json["Fullmsg"].stringValue
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
        
    }
}
