//
//  MyOrderVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class MyOrderVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrFutureOrderList: [JSON]?
    var arrPastOrderList: [JSON]?
    var intFutureOrder = Int()
    var intPastOrder = Int()
    var strFutureMsg = String()
    var strPastMsg = String()
    let upperRefresh = UIRefreshControl()
    var selectedHistoryType = selectMyOrderType.future
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblMyOrder: UITableView!
    
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        
        upperRefresh.addTarget(self, action: #selector(self.upperRefreshTable), for: .valueChanged)
        tblMyOrder.addSubview(upperRefresh)
        upperRefreshTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "My_orders_key").capitalized, type: .back, barType: .white)
    }

    @objc func upperRefreshTable()
    {
        if selectedHistoryType == .future {
            intFutureOrder = 0
        } else  {
            intPastOrder = 0
        }
        getOrderList()
    }    

}


//MARK:- Action Zone


//MARK:- Tableview Delegate & Datasource

extension MyOrderVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedHistoryType == .future {
            if arrFutureOrderList == nil{
                return 10
            }
            if arrFutureOrderList?.count == 0
            {
                let lbl = UILabel()
                lbl.text = strFutureMsg
                lbl.font = themeFont(size: 20, fontname: .medium)
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.black
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return arrFutureOrderList!.count
        }
        if arrPastOrderList == nil{
            return 10
        }
        if arrPastOrderList?.count == 0
        {
            let lbl = UILabel()
            lbl.text = strPastMsg
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrPastOrderList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCell") as! MyOrderCell
        cell.btnReorderOutlet.isHidden = true
        if arrFutureOrderList != nil || arrPastOrderList != nil {
            [cell.btnOrderStatusOutlet,cell.btnReorderOutlet].forEach { (btn) in
                btn?.isUserInteractionEnabled = true
            }
            var dict = JSON()
            if self.selectedHistoryType == .future {
                cell.vwDeleiveryDate.isHidden = false
                dict = arrFutureOrderList![indexPath.row]
                cell.lblDeliveryDate.text = "\(stringTodate(OrignalFormatter: "yyyy-MM-dd", YouWantFormatter: "dd-MM-yyyy", strDate: dict["delievry_dt"].stringValue)) \(dict["delievry_time"].stringValue)"
            } else {
                cell.vwDeleiveryDate.isHidden = true
                dict = arrPastOrderList![indexPath.row]
            }
            cell.btnReorderOutlet.tag = indexPath.row
            cell.btnOrderStatusOutlet.tag = indexPath.row
            cell.setupData(dict)
            cell.stopAnimationSkeleton()
        } else {
            [cell.btnOrderStatusOutlet,cell.btnReorderOutlet].forEach { (btn) in
                btn?.isUserInteractionEnabled = false
            }
        }
        
        return cell
    }
}

//MARK:- Action ZOne

extension MyOrderVC
{
    @IBAction func btnOrderStatusAction(_ sender:UIButton)
    {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderStatementVC") as! OrderStatementVC
        var dict = JSON()
        if selectedHistoryType == .future{
            dict = arrFutureOrderList![sender.tag]
        } else {
            dict = arrPastOrderList![sender.tag]
        }
        obj.dictOrder = dict
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnReOrderAction(_ sender:UIButton)
    {
        
    }
}

//MARK:- Scrollview delegate
extension MyOrderVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.selectedHistoryType == .future {
            if self.intFutureOrder != -1 &&  self.intFutureOrder != 0
            {
                if tblMyOrder.contentOffset.y >= (tblMyOrder.contentSize.height - tblMyOrder.bounds.size.height)
                {
                    getOrderList()
                }
            }
        } else {
            if self.intPastOrder != -1 &&  self.intPastOrder != 0
            {
                if tblMyOrder.contentOffset.y >= (tblMyOrder.contentSize.height - tblMyOrder.bounds.size.height)
                {
                    getOrderList()
                }
            }
        }
        
    }
}

//MARK:- Service

extension MyOrderVC
{
    func getOrderList()
    {
        if self.selectedHistoryType == .future {
            if intFutureOrder == -1 {
                upperRefresh.endRefreshing()
                return
            }
        } else {
            if intPastOrder == -1 {
                upperRefresh.endRefreshing()
                return
            }
        }
        
        upperRefresh.endRefreshing()
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kOrderHistory)"
            
            print("URL: \(url)")
            
            var param =  ["user_id":getUserDetail("id"),
                          "type":selectedHistoryType.rawValue
            ]
            
            if selectedHistoryType == .future {
                param["offset"]  = "\(intFutureOrder)"
            } else {
                param["offset"]  = "\(intPastOrder)"
            }
            
            print("Param : \(param)")
            
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        if self.selectedHistoryType == .future {
                            if(self.intFutureOrder == 0)
                            {
                                self.arrFutureOrderList = []
                            }
                            let array = json["data"].arrayValue
                            self.arrFutureOrderList = array + self.arrFutureOrderList!
                            self.intFutureOrder = json["next_offset"].intValue
                        } else {
                            if(self.intPastOrder == 0)
                            {
                                self.arrPastOrderList = []
                            }
                            let array = json["data"].arrayValue
                            self.arrPastOrderList = array + self.arrPastOrderList!
                            self.intPastOrder = json["next_offset"].intValue
                        }
                    }
                    else
                    {
                        if self.selectedHistoryType == .future {
                            self.arrFutureOrderList = []
                            self.strFutureMsg = json["msg"].stringValue
                            self.intFutureOrder = json["next_offset"].intValue
                        } else {
                            self.arrPastOrderList = []
                            self.strPastMsg = json["msg"].stringValue
                            self.intPastOrder = json["next_offset"].intValue
                        }
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblMyOrder.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

