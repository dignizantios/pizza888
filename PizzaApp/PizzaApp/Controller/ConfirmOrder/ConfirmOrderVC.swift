//
//  ConfirmOrderVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 24/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
//import BraintreeDropIn
//import Braintree

class ConfirmOrderVC: UIViewController {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblConfirmOrderTitle: UILabel!
    @IBOutlet weak var lblConfirmOrder: UILabel!
    @IBOutlet weak var lblPickupDetailTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblStorePhoneNumber: UILabel!
    @IBOutlet weak var lblStoreAddress: UILabel!
    @IBOutlet weak var txtViewComment: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var lblPaymentOptionTitle: UILabel!
    @IBOutlet weak var lblCreditCardTitle: UILabel!
    @IBOutlet weak var btnCheckedCreditCardOutlet: UIButton!
    @IBOutlet weak var btnCheckedCashPaymentOutlet: UIButton!
    @IBOutlet weak var btnCheckedPaypalOutlet: UIButton!
    @IBOutlet weak var btnNextOutlet: CustomButton!
    
    @IBOutlet weak var btnCreditCardOutlet: UIButton!
    @IBOutlet weak var btnCashPaymentOutlet: UIButton!
    @IBOutlet weak var btnPaypalOutlet: UIButton!
    @IBOutlet weak var lblCashPaymentTitle: UILabel!
    @IBOutlet weak var lblPaypalTitle: UILabel!
    
    //MARK:- Variable Declaration
    /*var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }*/
    var strFinalAmount : String = ""
    var payPalConfig = PayPalConfiguration()
    var dictDiscountAmt = JSON()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
      
//        setUpPaypal()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Confirm_order_key").capitalized, type: .back, barType: .white)
        
//        PayPalMobile.preconnect(withEnvironment: environment)
    }

}


//MARK:- Setup UI

extension ConfirmOrderVC
{
    func setupUI()
    {
        [lblConfirmOrderTitle,lblPickupDetailTitle,lblPaymentOptionTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        let orderUser =  Array(globalRealm.objects(CheckoutOrder.self))
        let objCheckOutOrder = orderUser[0]
        
        /*if objCheckOutOrder.strSelectedOrderType == 1 {
            lblPickupDetailTitle.text = getCommonString(key: "Delivery_detail_key").uppercased()
        } else {
            lblPickupDetailTitle.text = getCommonString(key: "Pickup_detail_key").uppercased()
        }*/
        
        lblPickupDetailTitle.text = getCommonString(key: "Store_detail_key").uppercased()
        
        lblConfirmOrderTitle.text = getCommonString(key: "Confirm_order_key").uppercased()
        
        lblPaymentOptionTitle.text = getCommonString(key: "Payment_option_key").uppercased()
        
        [lblCreditCardTitle,lblCashPaymentTitle,lblPaypalTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        lblCreditCardTitle.text = getCommonString(key: "Credit_card_key").capitalized
        lblCashPaymentTitle.text = getCommonString(key: "Cash_payment_key").capitalized
        lblPaypalTitle.text = getCommonString(key: "Paypal_key").capitalized
        
        [lblConfirmOrder].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        lblConfirmOrder.text = getCommonString(key: "Confirm_order_description_key")
        
        /*[lblUsername,lblPhonenumber].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = UIColor.black
        }*/
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_order_comments_key")
        }
        
        [txtViewComment].forEach { (txtView) in
            txtView?.font = themeFont(size: 17, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 5, left: 8, bottom: 8, right: 0)
        }
        
        btnNextOutlet.setUpThemeButtonUI()
        btnNextOutlet.backgroundColor = UIColor.appThemeRedColor
        btnNextOutlet.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        
        setUpData()
        
    }
    func setUpData()
    {
        objUser = GetUserData()
        objOrder = GetOrderData()
        
        let strMobileAtt =  attributedString(string1: "\(getCommonString(key: "Phone_key")) : ", string2: objOrder.strStorePhoneNumber, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        lblStorePhoneNumber.attributedText = strMobileAtt
        
        let strEmailAtt =  attributedString(string1: "\(getCommonString(key: "Email_key")) : ", string2: objOrder.strStoreAddress, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        lblStoreAddress.attributedText = strEmailAtt
        
        let strAddressAtt =  attributedString(string1: "\(getCommonString(key: "Address_key")) : ", string2: objOrder.strSelectStore, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        lblStoreName.attributedText = strAddressAtt
    }
    func setUpPaypal()
    {
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Developer Dignizant"//Give your company name here.
        payPalConfig.merchantPrivacyPolicyURL = URL(string:"https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        //This is the language in which your paypal sdk will be shown to users.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        //Here you can set the shipping address. You can choose either the address associated with PayPal account or different address. We’ll use .both here.
        payPalConfig.payPalShippingAddressOption = .none
    }
}
//MARK:- Other methods
extension ConfirmOrderVC : PayPalPaymentDelegate
{
    func OrderPaypal()
    {
        let item1 = PayPalItem(name: "Pizza", withQuantity: 2, withPrice: NSDecimalNumber(string: "0.50"), withCurrency: "USD", withSku: "Pizza-0037")
        
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items) //This is the total price of all the items
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "1.0")
        let tax = NSDecimalNumber(string: "1.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.adding(shipping).adding(tax) //This is the total price including shipping and tax
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Pizza888", intent: .sale)
        payment.items = items
        payment.paymentDetails = paymentDetails
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn’t be processable, and you’d want
            // to handle that here.
            print("Payment not processble: (payment)")
        }
    }
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        print("Completed - ",completedPayment)
        print("Completed payment - ",completedPayment.invoiceNumber)
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment :nn()nnSend this to your server for confirmation and fulfillment.")
        })
    }
    
    //Future payments
    func payPalFuturePaymentDidCancel(futurePaymentViewController: PayPalFuturePaymentViewController)
    {
        print("PayPal Future Payment Authorization Canceled")
    }
    
    func payPalFuturePaymentViewController(futurePaymentViewController: PayPalFuturePaymentViewController, didAuthorizeFuturePayment futurePaymentAuthorization: [NSObject : AnyObject])
    {
        print("PayPal Future Payment Authorization Success!")
    }
    
    func userDidCancelPayPalProfileSharingViewController(profileSharingViewController: PayPalProfileSharingViewController)
    {
        print("PayPal Profile Sharing Authorization Canceled")
    }
    
    func payPalProfileSharingViewController(profileSharingViewController: PayPalProfileSharingViewController, userDidLogInWithAuthorization profileSharingAuthorization: [NSObject : AnyObject])
    {
        print("PayPal Profile Sharing Authorization Success!")
        
    }
}

//MARK:- Textview Delegate

extension ConfirmOrderVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}



//MARK:- Action Zone

extension ConfirmOrderVC
{
    @IBAction func btnSelectPaymentOption(_ sender:UIButton)
    {
        [btnCheckedCreditCardOutlet,btnCheckedCashPaymentOutlet,btnCheckedPaypalOutlet].forEach { (btn) in
            btn?.isSelected = false
        }
        
        if sender == btnCreditCardOutlet
        {
            btnCheckedCreditCardOutlet.isSelected = true
        }
        else if  sender == btnCashPaymentOutlet
        {
            btnCheckedCashPaymentOutlet.isSelected = true
        }
        else if sender == btnPaypalOutlet{
            btnCheckedPaypalOutlet.isSelected = true
        }
        
    }
    
    @IBAction func btnNextAction(_ sender:UIButton)
    {
        //screen -> credit,
        //(1=credit,2=paypal,3=home
        objOrder.strComment = txtViewComment.text
        objOrder.strSelectPaymentOption = btnCheckedCreditCardOutlet.isSelected ? "1" : btnCheckedCashPaymentOutlet.isSelected ? "3" : btnCheckedPaypalOutlet.isSelected ? "2" : ""
        
        if(btnCheckedPaypalOutlet.isSelected)
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "OrderStatementVC") as! OrderStatementVC
            obj.selectedController = .paypal
            obj.dictDiscountAmt = dictDiscountAmt
            obj.strComment = self.txtViewComment.text ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
//            OrderPaypal()

        }
        else if(btnCheckedCashPaymentOutlet.isSelected)
        {
            if(objOrder.isCheckValidForOrder(objUser:objUser))
            {
                let dictFinal = GetCheckOutCartItem()
                CheckOutService(json : dictFinal)
                
            }else{
                makeToast(message: objOrder.strValidationMessage)
            }
            
        }
        else if btnCheckedCreditCardOutlet.isSelected {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "AddCardVC") as! AddCardVC
            obj.dictDiscountAmt = dictDiscountAmt
            obj.strComment = self.txtViewComment.text ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        
    }
}
//MARK:- Other methods
extension ConfirmOrderVC
{
    
    
    
}
//MARK:- Service
extension ConfirmOrderVC
{
    
    func CheckOutService(json : JSON)
    {
//        makeToast(message: "checkout API is remaining")
        print("Final order json - ",json)
    
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCheckout_cash)"
            
            print("URL: \(url)")
            
            let orderUser =  Array(globalRealm.objects(CheckoutOrder.self))
            let objCheckOutOrder = orderUser[0]
            
            let CheckUser = Array(globalRealm.objects(CheckoutUser.self))
            let objCheckOutUser = CheckUser[0]
            
            var param:[String:String] =  ["ordertype":"\(objCheckOutOrder.strSelectedOrderType)",
                          "address_name":objCheckOutUser.strName,
                          "address_phone":objCheckOutUser.strPhoneNumber,
                          "address_email":objCheckOutUser.strEmailAddress,
                          "tickemail":objCheckOutUser.strReceiveMail,
                          "ticksms":objCheckOutUser.strReceiveSMS,
                          "amount_total":dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) : getPriceFormatedValue(strPrice: CartTotalPrice()),
                          "future_date":objCheckOutOrder.strFutureOrderDate,
                          "future_time":objCheckOutOrder.strFutureOrderTime,
                          "prod_info":json.rawString() ?? "",
                          "customer_id":getUserDetail("id") != "" ? getUserDetail("id") : "0",
                          "disamnt":dictDiscountAmt.count != 0 ? dictDiscountAmt["disamt"].stringValue : "0",
                          "store_id":objCheckOutOrder.strSelectedStoreId,
                          "address_suburb":"",
                          "timestatus":"1",
                          "pay_type":selectPaymentType.cash.rawValue,
                          "note" : txtViewComment.text ?? "",
                          "order_prefix":strOrderPrefix,
                          
            ]
            
            if objCheckOutOrder.strSelectedOrderType == 1 { 
                
                param["unit_number"] = objCheckOutUser.strUnitNumber
                param["street_number"] = objCheckOutUser.strStreetNumber
                param["street_name"] = objCheckOutUser.strStreetName
                param["address_suburb"] = objCheckOutUser.strSuburbId
                param["delivery_instruction"] = objCheckOutUser.strDeliveryInsturction
                param["suburb_cost"] = objCheckOutUser.strSuburbCost
                param["amount_total"] = dictDiscountAmt.count != 0 ? getPriceFormatedValue(strPrice: (CartTotalPrice() - dictDiscountAmt["disamt"].floatValue) + Float(objCheckOutUser.strSuburbCost)!) : getPriceFormatedValue(strPrice: CartTotalPrice() + Float(objCheckOutUser.strSuburbCost)!)
            }
            
            if objCheckOutOrder.strFutureOrderDate != "" && objCheckOutOrder.strFutureOrderDate != ""
            {
                param["timestatus"] = "2"
            }
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.deleteAllItemsfromCart()
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderSucessVC") as! OrderSucessVC
                        obj.strMsg = json["Fullmsg"].stringValue
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }

    }
    
}
