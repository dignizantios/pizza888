//
//  HalfChildVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 08/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class HalfChildVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictData = JSON()
    var arrHalfCustomize:[JSON] = []
    var handleHalfCustomize:(Int,JSON) -> Void = {count,dict in}
    var index = 0
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var heightOfAddtoCart: NSLayoutConstraint!
    @IBOutlet weak var btnAddToCartOutlet: UIButton!
    @IBOutlet weak var viewOfBottom: UIView!
    @IBOutlet weak var tblHalfChild: UITableView!
    
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        getHalfList()
        
        self.heightOfAddtoCart.constant = 0
        self.btnAddToCartOutlet.isHidden = true
    }

}

//MARK:- Action Zone

extension HalfChildVC
{
    
    @IBAction func btnCustomizeAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        obj.dictData = arrHalfCustomize[sender.tag]
        obj.selectedCustomizedType = .half
//        obj.addCartDelegate = self
        obj.handledCustomize = {[weak self](dict) in
            for i in 0..<self!.arrHalfCustomize.count
            {
                var dict = self!.arrHalfCustomize[i]
                dict["is_selected"].stringValue = "0"
                self!.arrHalfCustomize[i] = dict
            }
            print("dict - ",dict)
            
            var dictInner = dict
            /*dictInner["name"].stringValue = dict["product_name"].stringValue
            dictInner["ingr_add"].stringValue = dict["ingr_add"].stringValue
            dictInner["ingr_name"].stringValue = dict["ingr_name"].stringValue
            dictInner["description"].stringValue = dict["description"].stringValue*/
            dictInner["is_selected"].stringValue = "1"
            self?.arrHalfCustomize[sender.tag] = dictInner
            self?.handleHalfCustomize((self?.index)!,(self?.arrHalfCustomize[sender.tag])!)
            self?.tblHalfChild.reloadData()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
   
}
//MARK:- Delegate

extension HalfChildVC:delegateToAddCart
{
    func setDelegateToAddCart(dict: JSON)
    {
        handleHalfCustomize(index,dict)

        
        /*for i in 0..<arrHalfCustomize.count
        {
            var dictData = arrHalfCustomize[i]
            if dictData["product_id"].stringValue == dict["product_id"].stringValue
            {
                dictData["is_customized"].stringValue = "1"
                arrHalfCustomize[i] = dictData
                addItemToCart(tag: i)
                break
            }
        }*/
    }
    
}
//MARK:- Tableview Delegate & Datasource

extension HalfChildVC :UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHalfCustomize.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HalfChildCell") as! HalfChildCell
        let dict = arrHalfCustomize[indexPath.row]
        cell.btnCustomizeOutelt.tag = indexPath.row
        cell.btnSelectHalfOutlet.tag = indexPath.row
        if dict["is_selected"].stringValue == "0"
        {
            cell.btnSelectHalfOutlet.isSelected = false
        }
        else
        {
            cell.btnSelectHalfOutlet.isSelected = true
        }
        
        cell.imgProduct.sd_setShowActivityIndicatorView(true)
        cell.imgProduct.sd_setIndicatorStyle(.gray)
        cell.imgProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_splash_holder"), options: .lowPriority, completed: nil)
        
        cell.lblHalfName.text = dict["product_name"].stringValue
        cell.lblPrice.text = "\(strCurrenrcy) \(dict["product_price"].stringValue)"
        
        cell.lblHalfDescription.text = dict["specification"].stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrHalfCustomize.count
        {
            var dict = arrHalfCustomize[i]
            dict["is_selected"].stringValue = "0"
            arrHalfCustomize[i] = dict
        }
        
        var dict = arrHalfCustomize[indexPath.row]
        dict["is_selected"].stringValue = "1"
        arrHalfCustomize[indexPath.row] = dict
        
        print("dict - ",dict)
        
        handleHalfCustomize(index,dict)
        
//        AddToCartManage()
        self.tblHalfChild.reloadData()
    }
    
    
}
//MARK:- Other methods
extension HalfChildVC
{
   
    
    func addAdditionalData()
    {
        for i in 0..<self.arrHalfCustomize.count
        {
            var dict = self.arrHalfCustomize[i]
            dict["is_customized"].stringValue = "0"
            dict["defaultTypeId"].stringValue = "1"
            dict["sauce_type"].stringValue =  ""
            dict["type"].stringValue =  "2"
            dict["pizzaSize"].stringValue = ""
            dict["extra_description"].stringValue =  ""
//                dict["category_id"] = JSON(dictProduct["category_id"])
//                dict["category_name"] = JSON(dictProduct["category_name"])
            self.arrHalfCustomize[i] = dict
        }
    
        self.tblHalfChild.reloadData()
    }
}
//MARK:- Service

extension HalfChildVC
{
    func getHalfList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kProductCustomize)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "type":dictData["type"].stringValue,
                          "store_id": dictStoreDetail["id"].stringValue,
                          "cst_id":dictData["cst_id"].stringValue]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrHalfCustomize = []
//                        self.arrHalfCustomize = json["sub_data"].arrayValue
                        if self.index == 0{
                            self.arrHalfCustomize = json["data"].arrayValue[0]["sub_data"].arrayValue[0]["items"].arrayValue
                        } else {
                            self.arrHalfCustomize = json["data"].arrayValue[0]["sub_data"].arrayValue[1]["items"].arrayValue
                        }
                        for i in 0..<self.arrHalfCustomize.count
                        {
                            var dictInner = self.arrHalfCustomize[i]
                            dictInner["is_selected"] = "0"
                            dictInner["product_half_price"].stringValue = self.dictData["product_price"].stringValue
                            self.arrHalfCustomize[i] = dictInner
                        }
                        self.addAdditionalData()
                    }
                    else
                    {
                        self.arrHalfCustomize = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblHalfChild.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

