//
//  OrderSucessVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 24/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit

class OrderSucessVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var strMsg = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnOKOutlet: CustomButton!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

       setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Order_success_key").capitalized, type: .withoutBack, barType: .white)
    }

}

//MARK:- Setup UI
extension OrderSucessVC
{
    func setupUI()  {
        
        [lblHeaderTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        
        [lblSubTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        lblHeaderTitle.text = getCommonString(key: "Order_success_header_key")
        
       /* let str1 = getCommonString(key: "Order_sucess_title_1_key")
        let str2 = getCommonString(key: "Order_sucess_title_2_key")
        let str3 = getCommonString(key: "Order_sucess_title_3_key")
        
        lblSubTitle.text = "\(str1) \n\n\(str2) \n\n\(str3)"*/
        lblSubTitle.text = strMsg
        
        btnOKOutlet.setUpThemeButtonUI()
        btnOKOutlet.backgroundColor = UIColor.appThemeRedColor
        btnOKOutlet.setTitle(getCommonString(key: "OK_key").uppercased(), for: .normal)
    }
}


//MARK:- Action Zone

extension OrderSucessVC
{
    @IBAction func btnOKAction(_ sender:UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigateUserWithLG(obj: obj)
    }
}
