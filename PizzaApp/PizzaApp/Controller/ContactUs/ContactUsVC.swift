//
//  ContactUsVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ContactUsVC: UIViewController {
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblFirstStoreAddress: UILabel!
    @IBOutlet weak var lblFirstStoreName: UILabel!
    @IBOutlet weak var vwGoogleMaps: GMSMapView!
    @IBOutlet weak var lblSelectStoreTitle: UILabel!
    @IBOutlet weak var lblSecondStoreAddress: UILabel!
    @IBOutlet weak var lblSecondStoreName: UILabel!
    @IBOutlet weak var lblThirdStoreAddress: UILabel!
    @IBOutlet weak var lblThirdStoreName: UILabel!
    @IBOutlet weak var btnFirstStoreOutlet: UIButton!
     @IBOutlet weak var btnSecondStoreOutlet: UIButton!
    @IBOutlet weak var btnThirdStoreOutlet: UIButton!
    @IBOutlet weak var vwStoreDetails: UIView!
    @IBOutlet weak var tblContactUsLocation: UITableView!
    
    //MARK:- Variable Declaration

    var arrHome : [JSON] = []
    var strMsg = String()
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
        
        getStoreList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Contact_us_key").capitalized, type: .sidemenu, barType: .clear)
    }

   
}

//MARK:- Setup UI
extension ContactUsVC
{
    func setupUI()  {
        
        [lblSelectStoreTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.black
        }
        
        [lblFirstStoreName,lblSecondStoreName,lblThirdStoreName].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblSelectStoreTitle.text = getCommonString(key: "Select_store_key").uppercased()
        
        lblFirstStoreName.text = getCommonString(key: "Cranbourne_west_key").capitalized
        lblSecondStoreName.text = getCommonString(key: "Cranbourne_north_key").capitalized
        lblThirdStoreName.text = getCommonString(key: "Balwyn_store_key").capitalized
        
        [lblFirstStoreAddress,lblSecondStoreAddress,lblThirdStoreAddress].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .medium)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        vwStoreDetails.isHidden = true
    }
}

//MARK:- Action ZOne

extension ContactUsVC
{
    @IBAction func btnStoreDetailAction(_ sender:UIButton)
    {
        if(arrHome.count < 3)
        {
            return
        }
        var index = 0
        if(sender == btnFirstStoreOutlet)
        {
            index = 0
        }
        else if(sender == btnSecondStoreOutlet)
        {
            index = 1
        }
        else if(sender == btnThirdStoreOutlet)
        {
            index = 2
        }
        
        let dict = arrHome[index]
        let obj = objStoryboard.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        obj.dict = dict
        obj.modalTransitionStyle = .coverVertical
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: true, completion:  nil)
    }
}
//MARK:- Other method
extension ContactUsVC
{
    func setUpStoreLocations()
    {
        if(arrHome.count >= 3)
        {
            vwStoreDetails.isHidden = false
            
            let dictStore1 = arrHome[0]
            lblFirstStoreName.text = dictStore1["store_name"].stringValue
            lblFirstStoreAddress.text = dictStore1["store_address"].stringValue
            setUpStorePins(dict:dictStore1)
            
            let dictStore2 = arrHome[1]
            lblSecondStoreName.text = dictStore2["store_name"].stringValue
            lblSecondStoreAddress.text = dictStore2["store_address"].stringValue
            setUpStorePins(dict:dictStore2)

            let dictStore3 = arrHome[2]
            lblThirdStoreName.text = dictStore3["store_name"].stringValue
            lblThirdStoreAddress.text = dictStore3["store_address"].stringValue
            setUpStorePins(dict:dictStore3)

        }
        
        
    }
    func setUpStorePins(dict:JSON)
    {
        
        let marker = GMSMarker()
        marker.title = dict["store_name"].stringValue
        marker.snippet = dict["store_address"].stringValue
        marker.icon = UIImage(named:"ic_pin_map_red")
        marker.position = CLLocationCoordinate2DMake(dict["lattitude"].doubleValue, dict["longitude"].doubleValue)
        marker.map = vwGoogleMaps
        
        let camera = GMSCameraPosition.camera(withLatitude: dict["lattitude"].doubleValue,
                                              longitude: dict["longitude"].doubleValue,
                                              zoom: 8)
        vwGoogleMaps.camera = camera
    }
}


//MARK:- UITablview cell Delegate

extension ContactUsVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrHome.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrHome.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsCell") as! ContactUsCell
        let dict = arrHome[indexPath.row]
        cell.setUpStoreLocations(dict: dict)
        setUpStorePins(dict: dict)
        let strMobileAtt =  attributedString(string1: "\(getCommonString(key: "Phone_key")) : ", string2: dict["store_phone"].stringValue, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        cell.lblStoreMobileNumber.attributedText = strMobileAtt
        
        let strEmailAtt =  attributedString(string1: "\(getCommonString(key: "Email_key")) : ", string2: dict["email"].stringValue, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        cell.lblEmailAddress.attributedText = strEmailAtt
        
        let strAddressAtt =  attributedString(string1: "\(getCommonString(key: "Address_key")) : ", string2: dict["store_address"].stringValue, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeLightGrayColor, font1: themeFont(size: 18, fontname: .medium), font2: themeFont(size: 18, fontname: .medium))
        cell.lblStoreAddress.attributedText = strAddressAtt
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrHome[indexPath.row]
        let obj = objStoryboard.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        obj.dict = dict
        obj.modalTransitionStyle = .coverVertical
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: true, completion:  nil)
    }
}

//MARK:- Service


extension ContactUsVC
{
    func getStoreList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kStoreList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrHome = []
                        self.arrHome = json["data"].arrayValue
                        self.vwStoreDetails.isHidden = false
//                        self.setUpStoreLocations()
                    }
                    else
                    {
                        self.vwStoreDetails.isHidden = false
                        self.arrHome = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblContactUsLocation.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}












