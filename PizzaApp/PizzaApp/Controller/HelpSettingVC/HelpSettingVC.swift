//
//  HelpSettingVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 26/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class HelpSettingVC: UIViewController {
    
    //MARK:- Variable declaration
    
    var arrHelpSetting:[JSON] = []
    
    //MARK:- Outlet Zone
  
    @IBOutlet weak var tblHelpSetting: UITableView!
    @IBOutlet weak var heightTblHelpSetting: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Help_setting_key").capitalized, type: .sidemenu, barType: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
     
        
    }
    
  

}

//MARK:- Array Setup

extension HelpSettingVC
{
    func setupOrderStatusArray() -> [JSON]
    {
        var arrMenu:[JSON] = []
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Terms_condition_key").capitalized
        dict["image"].stringValue = "ic_tnc_setting_screen"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Privacy_policy_key").capitalized
        dict["image"].stringValue = "ic_privacy_policy_setting_screen"

        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Delivery_policy_key").capitalized
        dict["image"].stringValue = "ic_delivery_policy_setting_screen"

        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Refund_policy_key").capitalized
        dict["image"].stringValue = "ic_return_policy_setting_screen"

        arrMenu.append(dict)
        
        return arrMenu
    }
}

//MARK:- Setup UI
extension HelpSettingVC
{
    func setupUI()
    {
        arrHelpSetting = setupOrderStatusArray()
        self.tblHelpSetting.reloadData()
        self.tblHelpSetting.tableFooterView = UIView()
    }
}


//MARK:- Tableview Delegate & Datasource

extension HelpSettingVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHelpSetting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountCell") as! MyAccountCell
        if indexPath.row == 0
        {
            cell.vwUpperSep.isHidden = false
        }
        else
        {
            cell.vwUpperSep.isHidden = true
        }
        cell.setData(arrHelpSetting[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrHelpSetting[indexPath.row]
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
        obj.dict = dict
        
        if(indexPath.row == 0)
        {
            obj.selectedPolicyType = .TermsCondition
        }
        else if(indexPath.row == 1)
        {
             obj.selectedPolicyType = .PrivacyPolicy
        }
        else if(indexPath.row == 2)
        {
            obj.selectedPolicyType = .DeliveryPolicy
        }
        else if(indexPath.row == 3)
        {
            obj.selectedPolicyType = .RefundPolicy
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
