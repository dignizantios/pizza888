//
//  StoreDetailVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class StoreDetailVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrTradingHours:[JSON] = []
    var dict = JSON()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblTradingHours: UILabel!
    @IBOutlet weak var tblTime: UITableView!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblPhoneTitle: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var txtViewAddess: UITextView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var btnSubmitOutlet: CustomButton!
    @IBOutlet weak var heightOftblTime: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        // Do any additional setup after loading the view.
        SetUpData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        tblTime.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
         sideMenuController?.isLeftViewSwipeGestureDisabled = true
    }
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblTime.removeObserver(self, forKeyPath: "contentSize")
        sideMenuController?.isLeftViewSwipeGestureDisabled = false
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblTime.contentSize.height)")
            self.heightOftblTime.constant = tblTime.contentSize.height
        }
    }

}
//MARK:- SetUpData
extension StoreDetailVC
{
    func SetUpData()
    {
        lblStoreName.text = dict["store_name"].stringValue
        
        arrTradingHours = dict["trading_hours"].arrayValue
        
        var arrayTrandingHours : [JSON] = []
         arrTradingHours.forEach { (json) in
            
            if(json["time"].stringValue != "")
            {
                arrayTrandingHours.append(json)
            }
            
        }
        arrTradingHours = arrayTrandingHours
        
        tblTime.reloadData()
    }
    
}
//MARK:- Tableview Delegate & Datasource

extension StoreDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTradingHours.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTimeCell") as! OrderTimeCell
        let dict = arrTradingHours[indexPath.row]
        let strAttText = attributedString(string1: "\(dict["days"].stringValue) :", string2: "\(dict["time"].stringValue)", color1: .white, color2: UIColor.white, font1: themeFont(size: 17, fontname: .semibold), font2: themeFont(size: 16, fontname: .regular))
        cell.lblTime.attributedText = strAttText
        return cell
    }
    
    
}

//MARK:- Setup UI

extension StoreDetailVC
{
    func setup()
    {
        lblStoreName.textColor = UIColor.white
        lblStoreName.font = themeFont(size: 20, fontname: .semibold)
        
        lblTradingHours.textColor = UIColor.white
        lblTradingHours.font = themeFont(size: 20, fontname: .regular)
        lblTradingHours.text = getCommonString(key: "Trading_hours_key").capitalized
        
        [lblNameTitle,lblPhoneTitle,lblEmailTitle,lblAddressTitle].forEach { (lbl) in
                lbl?.font = themeFont(size: 16, fontname: .medium)
                lbl?.textColor = .black
        }
        
        [txtName,txtPhone,txtEmail].forEach { (txtField) in
            txtField?.font = themeFont(size: 16, fontname: .regular)
            txtField?.textColor = .black
            txtField?.delegate = self
            txtField?.placeholder = getCommonString(key: "Enter_here_key")
        }
        
        lblNameTitle.text = getCommonString(key: "Name_key").uppercased()
        lblPhoneTitle.text = getCommonString(key: "Phone_key").uppercased()
        lblEmailTitle.text = getCommonString(key: "Email_key").uppercased()
        lblAddressTitle.text = getCommonString(key: "Message_key").uppercased()
        
        lblPlaceholder.textColor = UIColor.appThemeLightGrayColor
        lblPlaceholder.font = themeFont(size: 16, fontname: .regular)
        lblPlaceholder.text = getCommonString(key: "Enter_here_key")
        
        [txtViewAddess].forEach { (txtView) in
            txtView?.font = themeFont(size: 16, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        }
        
        btnSubmitOutlet.setUpThemeButtonUI()
        btnSubmitOutlet.setTitle(getCommonString(key: "Next_key"), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtPhone)
        
        
    }
}

//MARK:- Action Zone

extension StoreDetailVC
{
    @IBAction func btnCloseAction(_ sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitAction(_ sender:UIButton)
    {
        var strValidationMessage = ""
        if(txtName.text?.trimmed() == "")
        {
            strValidationMessage = getValidationString(key: "Please_enter_name_key")
            makeToast(message: strValidationMessage)
            return
        }
        else if(txtEmail.text?.trimmed() == "")
        {
            strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            makeToast(message: strValidationMessage)
            return
        }
        else if txtEmail.text?.isValidEmail() == false
        {
            strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            makeToast(message: strValidationMessage)
            return
        }
        else if(txtPhone.text?.trimmed() == "")
        {
            strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            makeToast(message: strValidationMessage)
            return
        }
        else if(txtViewAddess.text?.trimmed() == "")
        {
            strValidationMessage = getValidationString(key: "Please_enter_description_key")
            makeToast(message: strValidationMessage)
            return
        }
        ContactUsService()
    }
}

//MARK:- Textfiled Delegate

extension StoreDetailVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- Textview Delegate

extension StoreDetailVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Service

extension StoreDetailVC
{
    func getTradingHours()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kTradingHours)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrTradingHours = []
                        self.arrTradingHours = json["data"].arrayValue
                    }
                    else
                    {
                        self.arrTradingHours = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblTime.reloadData()
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    func ContactUsService()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kContactUs)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                "name" : txtName.text ?? "",
                "email" : txtEmail.text ?? "",
                "phone" : txtPhone.text ?? "",
                "message" : txtViewAddess.text ?? ""]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}
