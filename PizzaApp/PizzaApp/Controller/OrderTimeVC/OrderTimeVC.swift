//
//  OrderTimeVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class OrderTimeVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrTimes:[JSON] = []
    var arrTradingHours:[JSON] = []
    var isOpenStore = Bool()
    var strShopStatusMsg = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblASAPTitle: UILabel!
    @IBOutlet weak var viewOfASAP: UIView!
    @IBOutlet weak var lblAboveTitle: UILabel!
    @IBOutlet weak var tblOrderTime: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewOfFutureOrder: UIView!
    @IBOutlet weak var lblFutureOrder: UILabel!
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getTimes()
        getTradingHours()
        getStoreOpenClose()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Order_time_key"), type: .backWithCart, barType: .white)
         tblOrderTime.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblOrderTime.removeObserver(self, forKeyPath: "contentSize")
        //self.navigationController?.navigationBar.removeFromSuperview()
    }
    
    override func viewDidLayoutSubviews() {
        [viewOfASAP,viewOfFutureOrder].forEach { (view) in
            view?.roundCorners([.topRight,.bottomRight], radius: 30)
        }
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblOrderTime.contentSize.height)")
            self.heightOfTable.constant = tblOrderTime.contentSize.height
        }
    }


}

//MARK:- Setup UI

extension  OrderTimeVC
{
    func setup()  {
        
        self.viewOfASAP.alpha = 0.5
        
        [lblTitle,lblAboveTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = getCommonString(key: "Trading_hours_key").capitalized
        lblAboveTitle.text = getCommonString(key: "When_would_you_like_your_order_key")
        
        [lblASAPTitle,lblFutureOrder].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }
        
        lblASAPTitle.text = getCommonString(key: "ASAP_key")
        lblFutureOrder.text = getCommonString(key: "Future_order_key").capitalized
        
    }
    
}

//MARK:- Tableview Delegate & Datasource

extension OrderTimeVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTradingHours.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTimeCell") as! OrderTimeCell       
        let dict = arrTradingHours[indexPath.row]
        let strAttText = attributedString(string1: "\(dict["days"].stringValue) :", string2: "\(dict["time"].stringValue)", color1: .black, color2: UIColor.appThemeDarkGrayColor, font1: themeFont(size: 17, fontname: .regular), font2: themeFont(size: 16, fontname: .regular))
        cell.lblTime.attributedText = strAttText
        return cell
    }
    
   
}

//MARK:- Action ZOne

extension OrderTimeVC
{
    @IBAction func btnASAPAction(_ sender:UIButton)
    {
        if isOpenStore == true
        {
            objOrder.strSelectedPickUpTime = .asap
            objOrder.strFutureOrderDate = ""
            objOrder.strFutureOrderTime = ""
            SaveOrderDetailsToDefaults()
            let obj = objStoryboard.instantiateViewController(withIdentifier: "SelectItemsVC") as! SelectItemsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
           makeToast(message: strShopStatusMsg)
        }
       
    }
    
    @IBAction func btnFutureOrderAction(_ sender:UIButton)
    {

        let obj = SelectFutureDateVC()
        obj.arrTimes = arrTimes
        obj.selectDateTime = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)        
    }
    
}

//MARK:- Select Date&Time

extension OrderTimeVC:delegateSelectFutureDate
{
    func selectedDateTime(selectedDate: String, selectedTime: String)
    {
        objOrder.strSelectedPickUpTime = .future

        print("selected date \(selectedDate) \n selectedTime \(selectedTime)")
        objOrder.strFutureOrderDate = selectedDate
        objOrder.strFutureOrderTime = selectedTime
        SaveOrderDetailsToDefaults()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "SelectItemsVC") as! SelectItemsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}


//MARK:- Service

extension OrderTimeVC
{
    func getTimes()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetTimes)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id": dictStoreDetail["id"].stringValue
                          ]            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrTimes = []
                        self.arrTimes = json["data"].arrayValue
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    
    func getStoreOpenClose()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kStoreOpenClose)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id": dictStoreDetail["id"].stringValue
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.isOpenStore = true
                        self.viewOfASAP.alpha = 1
                    }
                    else
                    {
                        self.strShopStatusMsg = json["msg"].stringValue
                        self.isOpenStore = false
                        self.viewOfASAP.alpha = 0.5
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    
    func getTradingHours()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kTradingHours)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id": dictStoreDetail["id"].stringValue
                        
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrTradingHours = []
                        self.arrTradingHours = json["data"].arrayValue                        
                    }
                    else
                    {
                        self.arrTradingHours = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblOrderTime.reloadData()
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

